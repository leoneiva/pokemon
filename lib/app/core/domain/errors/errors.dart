abstract class Failure implements Exception {
  String? get message;

  @override
  String toString() {
    return message!;
  }
}

class InternalError implements Failure {
  @override
  final String? message;

  InternalError({this.message});
}

class GetInformationError extends Failure {
  @override
  final String? message;
  GetInformationError({
    this.message,
  });
}

class TimeOutError extends Failure {
  @override
  final String? message;
  TimeOutError({
    this.message,
  });
}

class NotFoundError extends Failure {
  @override
  final String? message;
  NotFoundError({
    this.message,
  });
}
