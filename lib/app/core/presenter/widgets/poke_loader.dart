import 'package:flutter/material.dart';

import '../../constants/poke_colors.dart';

class PokeLoader {
  static Widget loading({Color? color}) {
    return Center(
      child: CircularProgressIndicator(
        valueColor:
            AlwaysStoppedAnimation<Color>(color ?? PokeColors.primaryColor),
      ),
    );
  }
}
