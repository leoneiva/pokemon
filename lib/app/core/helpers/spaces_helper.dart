import 'package:flutter/material.dart';

class SpacesHelper {
  static Widget w(double size) {
    return SizedBox(width: size);
  }

  static Widget h(double size) {
    return SizedBox(height: size);
  }
}
