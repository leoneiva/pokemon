class HelperPokemon {
  static int getIdFromUrl({required String url}) {
    final String last3characters = url.substring(url.length - 3);
    final pokemonId = last3characters.replaceAll('/', '');
    return int.parse(pokemonId);
  }

  static String getImageFromUrl({required String url}) {
    return 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/${getIdFromUrl(url: url)}.png';
  }
}
