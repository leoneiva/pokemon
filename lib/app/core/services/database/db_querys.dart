enum Querys {
  createTableComment,
  createTableRating,
}

enum Tables {
  commentTable,
  ratingTable,
}

enum Columns {
  // Comments
  colCoId,
  colCoComment,
  colCoPokemonId,
  // Rating
  colRaId,
  colRaRating,
  colRaPokemonId,
}

extension TablesExtension on Tables {
  static final _name = {
    Tables.commentTable: 'comment',
    Tables.ratingTable: 'rating',
  };

  String get name => _name[this]!;
}

extension ColumnsExtension on Columns {
  static final _name = {
    // Comments
    Columns.colCoId: 'id',
    Columns.colCoComment: 'comment',
    Columns.colCoPokemonId: 'pokemonId',
    // Rating
    Columns.colRaId: 'id',
    Columns.colRaRating: 'rating',
    Columns.colRaPokemonId: 'pokemonId',
  };

  String get name => _name[this]!;
}

extension QuerysExtension on Querys {
  static final _query = {
    Querys.createTableComment:
        'CREATE TABLE ${Tables.commentTable.name} (${Columns.colCoId.name} INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ${Columns.colCoComment.name} TEXT NOT NULL, ${Columns.colCoPokemonId.name} INT NOT NULL)',
    Querys.createTableRating:
        'CREATE TABLE ${Tables.ratingTable.name} (${Columns.colRaId.name} INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ${Columns.colRaRating.name} INT NOT NULL, ${Columns.colRaPokemonId.name} INT NOT NULL)',
  };

  String get query => _query[this]!;
}
