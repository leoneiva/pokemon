// ignore_for_file: unrelated_type_equality_checks

import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/rating_entity.dart';
import 'package:sqflite/sqflite.dart';

import '../../../features/pokemon/infra/models/rating_model.dart';
import '../../../features/pokemon/infra/models/send_comment_model.dart';
import '../../../features/pokemon/infra/models/send_rating_model.dart';
import '../../../features/pokemon/infra/models/comment_model.dart';
import 'db_querys.dart';

class DBService {
  static DBService? _dbService;
  static Database? _database;

  DBService._createInstance();
  factory DBService() {
    _dbService ??= DBService._createInstance();
    return _dbService!;
  }

  Future<Database> get database async {
    _database ??= await initializeDatabase();
    return _database!;
  }

  Future<Database> initializeDatabase() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = '${directory.path}pokemon.db';

    var pokemonDatabase =
        await openDatabase(path, version: 1, onCreate: _createDb);
    return pokemonDatabase;
  }

  Future<void> _createDb(Database db, int version) async {
    await db.execute(Querys.createTableComment.query);
    await db.execute(Querys.createTableRating.query);
  }

  // Insert
  Future<int> setComment({required SendCommentModel comment}) async {
    Database db = await database;
    var result = await db.insert(Tables.commentTable.name, comment.toMap());
    return result;
  }

  Future<int> setRating({required SendRatingModel rating}) async {
    Database db = await database;
    var result = await db.insert(Tables.ratingTable.name, rating.toMap());
    return result;
  }

  // Update
  Future<int> putRating({required SendRatingModel rating}) async {
    Database db = await database;
    var result = await db.update(Tables.ratingTable.name, rating.toMap(),
        where: '${Columns.colRaId.name} = ?', whereArgs: [rating.id]);
    return result;
  }

  // Get
  Future<List<CommentModel>> getComments({required int idPokemon}) async {
    try {
      Database db = await database;
      List<Map> consult = await db.query(
        Tables.commentTable.name,
        columns: [
          Columns.colCoId.name,
          Columns.colCoComment.name,
          Columns.colCoPokemonId.name,
        ],
        where: '${Columns.colCoPokemonId.name} = ?',
        whereArgs: [idPokemon],
      );
      List<CommentModel> comments = [];
      if (consult.isNotEmpty) {
        for (var element in consult) {
          comments.add(CommentModel.fromMap(element));
        }
      }
      return comments;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<Map>?> getRating({required int idPokemon}) async {
    try {
      Database db = await database;
      return await db.query(
        Tables.ratingTable.name,
        columns: [
          Columns.colRaId.name,
          Columns.colRaRating.name,
          Columns.colRaPokemonId.name,
        ],
        where: '${Columns.colRaPokemonId.name} = ?',
        whereArgs: [idPokemon],
      );
    } catch (e) {
      rethrow;
    }
  }

  Future<bool> cleanAllRecords() async {
    Database db = await database;
    await db.delete(Tables.commentTable.name);
    await db.delete(Tables.ratingTable.name);
    return true;
  }

  Future<void> closeDB() async {
    Database db = await database;
    await db.close();
  }
}
