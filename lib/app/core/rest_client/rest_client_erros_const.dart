class RestClientErrors {
  static const notFoundError = 404;
  static const timeOutError = 408;
  static const internalServerError = 500;
}
