enum HttpStatusCode {
  information,
  success,
  redirect,
  clientError,
  serverError;

  factory HttpStatusCode.fromCode(int code) {
    if (code < 100 || code > 599) {
      throw Exception("Invalid status code");
    }

    return HttpStatusCode.values.elementAt((code ~/ 100) - 1);
  }
}
