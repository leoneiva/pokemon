import 'package:flutter_modular/flutter_modular.dart';

class CustomNavigator {
  Future pushNamed({required String route, dynamic args}) {
    return Modular.to.pushNamed(route, arguments: args);
  }

  Future pushReplacementNamed({required String route, dynamic args}) {
    return Modular.to.pushReplacementNamed(route, arguments: args);
  }

  pop() {
    return Modular.to.pop();
  }
}
