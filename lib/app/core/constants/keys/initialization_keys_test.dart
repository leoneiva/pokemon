enum AppKeys {
  background,
  logo,
  pokeCard,
}

extension AppTest on AppKeys {
  static final _path = {
    AppKeys.background: 'background',
    AppKeys.logo: 'logo',
    AppKeys.pokeCard: 'pokeCard',
  };

  String get path => _path[this]!;
}
