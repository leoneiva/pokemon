enum Api {
  base,
  pokemons,
  pokemon,
  pokemonDescription,
}

extension RoutesExtension on Api {
  static final _path = {
    Api.base: 'www.pokeapi.co',
    Api.pokemons: '/api/v2/pokemon/',
    Api.pokemon: '/api/v2/pokemon/',
    Api.pokemonDescription: '/api/v2/ability/',
  };

  String get path => _path[this]!;
}
