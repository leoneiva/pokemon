enum Routes {
  // Routes
  splash,
  dashboard,
  pokemon,
  // Route Not Named
  notNamed,
}

extension RoutesExtension on Routes {
  static final _path = {
    // Routes
    Routes.splash: '/',
    Routes.dashboard: '/dashboard',
    Routes.pokemon: '/pokemon',
    // Route Not Named
    Routes.notNamed: '/routes_not_named',
  };

  String get path => _path[this]!;
}
