import 'package:flutter/material.dart';

class PokeColors {
  static get white => const Color(0xffffffff);
  static get dark => const Color(0xff212121);
  static get primaryColor => const Color(0xff70559B);
  static get disableColor => const Color.fromARGB(255, 143, 133, 158);
}
