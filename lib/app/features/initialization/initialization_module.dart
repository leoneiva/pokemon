import 'package:flutter_modular/flutter_modular.dart';
import 'package:pokemon/app/core/utils/custom_navigator.dart';
import '../../core/constants/routes.dart';
import 'presenter/pages/splash_page.dart';

class InitializationModule extends Module {
  @override
  List<Bind<Object>> get binds => [
        Bind.factory(
          (i) => CustomNavigator(),
        ),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(
          Routes.splash.path,
          child: (context, __) => SplashPage(
            navigator: context.read<CustomNavigator>(),
          ),
        ),
      ];
}
