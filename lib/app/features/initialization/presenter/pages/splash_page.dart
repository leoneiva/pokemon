import 'package:flutter/material.dart';
import 'package:pokemon/app/core/utils/custom_navigator.dart';
import '../../../../core/constants/routes.dart';
import '../../../../core/constants/keys/initialization_keys_test.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key, required this.navigator}) : super(key: key);

  final CustomNavigator navigator;

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    Future.delayed(
      const Duration(seconds: 3),
      () => widget.navigator.pushReplacementNamed(route: Routes.dashboard.path),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Image.asset(
              key: Key(AppKeys.background.path),
              'assets/images/splash_background.png',
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              fit: BoxFit.cover,
            ),
            Column(
              children: [
                Expanded(
                  child: Center(
                    child: Image.asset(
                      key: Key(AppKeys.logo.path),
                      'assets/images/logo_splash.png',
                      width: MediaQuery.of(context).size.width * 0.8,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
