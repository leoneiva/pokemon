import '../../../../core/rest_client/rest_client_erros_const.dart';
import '../../../../core/rest_client/rest_client_exception.dart';
import '../../domain/entities/get_pokemons_entity.dart';
import '../../domain/entities/pokemons_entity.dart';
import '../../domain/repositories/get_pokemons_repository.dart';
import '../datasources/get_pokemons_datasource.dart';
import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';

class GetPokemonsRepositoryImpl implements GetPokemonsRepository {
  final GetPokemonsDataSource _dataSource;

  GetPokemonsRepositoryImpl(this._dataSource);

  @override
  Future<Either<Failure, PokemonsEntity>> getPokemons({
    required GetPokemonsEntity params,
  }) async {
    try {
      final result = await _dataSource.getPokemons(params: params);
      return Right(result);
    } on RestClientException catch (e) {
      final code = e.statusCode;
      final message = e.message;

      switch (code) {
        case RestClientErrors.internalServerError:
          return Left(InternalError(message: message));
        case RestClientErrors.timeOutError:
          return Left(TimeOutError(message: message));
        case RestClientErrors.notFoundError:
          return Left(NotFoundError(message: message));
        default:
          rethrow;
      }
    }
  }
}
