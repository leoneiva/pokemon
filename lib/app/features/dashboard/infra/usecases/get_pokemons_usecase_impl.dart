import '../../domain/entities/get_pokemons_entity.dart';
import '../../domain/entities/pokemons_entity.dart';
import '../../domain/repositories/get_pokemons_repository.dart';
import '../../domain/usecases/get_pokemons_usecase.dart';
import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';

class GetPokemonsUsecaseImpl extends GetPokemonsUsecase {
  final GetPokemonsRepository _repository;
  GetPokemonsUsecaseImpl(this._repository);

  @override
  Future<Either<Failure, PokemonsEntity>> call({
    required GetPokemonsEntity params,
  }) async {
    return await _repository.getPokemons(params: params);
  }
}
