class GetPokemonsModel {
  int? limit;
  int? offset;

  GetPokemonsModel({this.limit, this.offset});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['limit'] = limit.toString();
    data['offset'] = offset.toString();
    return data;
  }
}
