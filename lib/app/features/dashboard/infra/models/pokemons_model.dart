import 'dart:convert';

import 'package:pokemon/app/features/dashboard/domain/entities/pokemons_entity.dart';

class PokemonsModel extends PokemonsEntity {
  PokemonsModel.fromJson(String data) {
    Map<String, dynamic> json = jsonDecode(data);
    count = json['count'];
    next = json['next'];
    previous = json['previous'];
    if (json['results'] != null) {
      results = <PokemonModel>[];
      json['results'].forEach((v) {
        results!.add(PokemonModel.fromJson(v));
      });
    }
  }
}

class PokemonModel extends PokemonEntity {
  PokemonModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }
}
