import 'dart:async';

import '../../domain/entities/get_pokemons_entity.dart';
import '../models/pokemons_model.dart';

abstract class GetPokemonsDataSource {
  Future<PokemonsModel> getPokemons({
    required GetPokemonsEntity params,
  });
}
