import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../entities/get_pokemons_entity.dart';
import '../entities/pokemons_entity.dart';

abstract class GetPokemonsRepository {
  Future<Either<Failure, PokemonsEntity>> getPokemons({
    required GetPokemonsEntity params,
  });
}
