import 'package:dartz/dartz.dart';

import '../../../../core/domain/errors/errors.dart';
import '../entities/get_pokemons_entity.dart';
import '../entities/pokemons_entity.dart';

abstract class GetPokemonsUsecase {
  Future<Either<Failure, PokemonsEntity>> call({
    required GetPokemonsEntity params,
  });
}
