class PokemonsEntity {
  int? count;
  String? next;
  String? previous;
  List<PokemonEntity>? results;

  PokemonsEntity({this.count, this.next, this.previous, this.results});
}

class PokemonEntity {
  String? name;
  String? url;

  PokemonEntity({this.name, this.url});
}
