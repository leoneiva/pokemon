// ignore_for_file: overridden_fields

import '../../infra/models/get_pokemons_model.dart';

class GetPokemonsEntity extends GetPokemonsModel {
  @override
  int? limit;
  @override
  int? offset;

  GetPokemonsEntity({this.limit, this.offset});
}
