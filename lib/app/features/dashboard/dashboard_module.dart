import 'package:flutter_modular/flutter_modular.dart';
import 'package:http/http.dart';
import 'package:pokemon/app/core/utils/custom_navigator.dart';

import '../../core/constants/routes.dart';
import 'domain/repositories/get_pokemons_repository.dart';
import 'domain/usecases/get_pokemons_usecase.dart';
import 'external/datasources/get_pokemons_datasource_impl.dart';
import 'infra/datasources/get_pokemons_datasource.dart';
import 'infra/repositories/get_pokemons_repository_impl.dart';
import 'infra/usecases/get_pokemons_usecase_impl.dart';
import 'presenter/controllers/dashboard_controller.dart';
import 'presenter/pages/dashboard_page.dart';

class DashboardModule extends Module {
  @override
  List<Bind<Object>> get binds => [
        // Others
        Bind.factory(
          (i) => CustomNavigator(),
        ),
        //Services
        Bind.factory((i) => Client()),
        // UseCases
        Bind.lazySingleton(
          (i) => GetPokemonsUsecaseImpl(
            i.get<GetPokemonsRepository>(),
          ),
        ),
        // Repositories
        Bind.factory(
          (i) => GetPokemonsRepositoryImpl(
            i.get<GetPokemonsDataSource>(),
          ),
        ),
        // Datasources
        Bind.factory(
          (i) => GetPokemonsDataSourceImpl(i.get<Client>()),
        ),
        // Controllers
        Bind.factory(
          (i) => DashboardController(
            i.get<GetPokemonsUsecase>(),
          ),
        ),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(
          Routes.dashboard.path,
          child: (context, __) => DashboardPage(
            controller: context.read<DashboardController>(),
            navigator: context.read<CustomNavigator>(),
          ),
          transition: TransitionType.rightToLeft,
        ),
      ];
}
