import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/constants/poke_colors.dart';

class PokeAppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final String image;

  const PokeAppBarWidget({Key? key, required this.title, required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 5.w, top: 5.w),
      child: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: PokeColors.primaryColor),
        elevation: 0,
        title: Row(
          children: [
            Image.asset(
              image,
              height: 40.r,
              color: PokeColors.primaryColor,
            ),
            SizedBox(
              width: 8.w,
            ),
            Text(
              title,
              style: TextStyle(
                color: PokeColors.primaryColor,
                fontWeight: FontWeight.w700,
                fontSize: 24.sp,
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(56.h);
}
