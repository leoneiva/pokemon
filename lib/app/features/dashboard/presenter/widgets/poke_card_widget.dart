import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/constants/poke_colors.dart';
import '../../../../core/presenter/widgets/poke_loader.dart';

class PokeCardWidget extends StatelessWidget {
  const PokeCardWidget({
    super.key,
    required this.pokemonName,
    required this.pokemonImage,
    required this.onTap,
  });

  final String pokemonName;
  final String pokemonImage;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(),
      child: Container(
        decoration: BoxDecoration(
          color: PokeColors.white,
          border: Border.all(
            color: PokeColors.primaryColor,
          ),
          borderRadius: BorderRadius.circular(9.0.r),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 0,
              blurRadius: 4,
              offset: const Offset(0, 4),
            ),
          ],
        ),
        height: 129.h,
        width: 113.w,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.network(pokemonImage,
                    loadingBuilder: (context, child, loadingProgress) {
                  if (loadingProgress == null) return child;
                  return Center(
                    child: PokeLoader.loading(),
                  );
                }),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(8.r),
                  bottomLeft: Radius.circular(8.r),
                ),
                color: PokeColors.primaryColor,
              ),
              height: 41.h,
              child: Center(
                child: Text(
                  textAlign: TextAlign.center,
                  pokemonName,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.sp,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
