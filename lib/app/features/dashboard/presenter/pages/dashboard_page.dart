import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pokemon/app/core/utils/custom_navigator.dart';
import 'package:scroll_edge_listener/scroll_edge_listener.dart';
import '../../../../core/constants/keys/initialization_keys_test.dart';
import '../../../../core/constants/poke_colors.dart';
import '../../../../core/constants/routes.dart';
import '../../../../core/helpers/pokemon_helper.dart';
import '../../../../core/presenter/widgets/poke_loader.dart';
import '../../../pokemon/domain/entities/pokemon_page_entity.dart';
import '../controllers/dashboard_controller.dart';
import '../widgets/poke_app_bar_widget.dart';
import '../widgets/poke_card_widget.dart';

class DashboardPage extends StatefulWidget {
  final DashboardController controller;
  final CustomNavigator navigator;
  const DashboardPage({
    Key? key,
    required this.controller,
    required this.navigator,
  }) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  void initState() {
    super.initState();
    widget.controller.getPokemons();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: PokeColors.white,
        body: Observer(
          builder: (_) => Column(
            children: [
              const PokeAppBarWidget(
                image: 'assets/images/pokeball.png',
                title: 'PokéBot',
              ),
              Visibility(
                visible: widget.controller.isLoading,
                child: Expanded(
                  child: PokeLoader.loading(),
                ),
              ),
              Visibility(
                visible: !widget.controller.isLoading,
                child: Expanded(
                  child: ScrollEdgeListener(
                    edge: ScrollEdge.end,
                    edgeOffset: 0,
                    debounce: const Duration(milliseconds: 500),
                    listener: () => widget.controller.loadMorePokemons(),
                    child: GridView.builder(
                      padding: EdgeInsets.only(
                        top: 10.w,
                        left: 10.w,
                        right: 10.w,
                        bottom: 40.w,
                      ),
                      itemCount: widget.controller.pokemons.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: (3),
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                      ),
                      itemBuilder: (BuildContext context, int index) {
                        return PokeCardWidget(
                          key: Key(AppKeys.pokeCard.path),
                          pokemonName: widget.controller.pokemons[index].name!,
                          pokemonImage: HelperPokemon.getImageFromUrl(
                            url: widget.controller.pokemons[index].url!,
                          ),
                          onTap: () => widget.navigator.pushNamed(
                            route: Routes.pokemon.path,
                            args: PokemonPageEntity(
                              id: HelperPokemon.getIdFromUrl(
                                url: widget.controller.pokemons[index].url!,
                              ),
                              image: HelperPokemon.getImageFromUrl(
                                url: widget.controller.pokemons[index].url!,
                              ),
                              name: widget.controller.pokemons[index].name!,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: widget.controller.isLoadingMore,
                child: Padding(
                  padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                  child: PokeLoader.loading(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
