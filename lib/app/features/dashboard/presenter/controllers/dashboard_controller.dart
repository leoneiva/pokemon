import 'package:mobx/mobx.dart';

import '../../../../core/domain/errors/errors.dart';
import '../../domain/entities/get_pokemons_entity.dart';
import '../../domain/entities/pokemons_entity.dart';
import '../../domain/usecases/get_pokemons_usecase.dart';

part 'dashboard_controller.g.dart';

class DashboardController = DashboardControllerBase with _$DashboardController;

abstract class DashboardControllerBase with Store {
  final GetPokemonsUsecase getPokemonsUsecase;

  DashboardControllerBase(this.getPokemonsUsecase);

  int offset = 0;
  int limit = 18;

  @observable
  bool isLoading = true;

  @observable
  bool isLoadingMore = false;

  @observable
  List<PokemonEntity> pokemons = [];

  @action
  Future<void> getPokemons() async {
    isLoading = true;
    try {
      final result = await getPokemonsUsecase(
        params: GetPokemonsEntity(
          limit: limit,
          offset: offset,
        ),
      );
      result.fold(
        (f) async {
          if (f is NotFoundError) {
          } else if (f is TimeOutError) {
          } else if (f is InternalError) {}
        },
        (s) async {
          pokemons = s.results!;
        },
      );
      isLoading = false;
    } catch (error) {
      isLoading = false;
    }
  }

  @action
  Future<void> loadMorePokemons() async {
    isLoadingMore = true;
    try {
      offset = offset + limit;
      final result = await getPokemonsUsecase(
        params: GetPokemonsEntity(
          limit: limit,
          offset: offset,
        ),
      );
      result.fold(
        (f) async {
          if (f is NotFoundError) {
          } else if (f is TimeOutError) {
          } else if (f is InternalError) {}
        },
        (s) async {
          pokemons.addAll(s.results!);
        },
      );
      isLoadingMore = false;
    } catch (error) {
      isLoadingMore = false;
    }
  }
}
