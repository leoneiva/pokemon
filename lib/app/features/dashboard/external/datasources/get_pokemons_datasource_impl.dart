import 'package:http/http.dart';
import '../../../../core/constants/api.dart';
import '../../domain/entities/get_pokemons_entity.dart';
import '../../infra/datasources/get_pokemons_datasource.dart';
import '../../infra/models/pokemons_model.dart';

class GetPokemonsDataSourceImpl implements GetPokemonsDataSource {
  final Client _http;
  GetPokemonsDataSourceImpl(this._http);

  @override
  Future<PokemonsModel> getPokemons({
    required GetPokemonsEntity params,
  }) async {
    Response response = await _http.get(
      Uri.https(
        Api.base.path,
        Api.pokemons.path,
        params.toJson(),
      ),
    );
    return PokemonsModel.fromJson(response.body);
  }
}
