import 'package:flutter_modular/flutter_modular.dart';
import 'package:pokemon/app/core/utils/custom_navigator.dart';

import '../../core/constants/routes.dart';
import '../../core/services/database/db_service.dart';
import 'domain/entities/pokemon_page_entity.dart';
import 'domain/repositories/get_comments_repository.dart';
import 'domain/repositories/get_pokemon_description_repository.dart';
import 'domain/repositories/get_pokemon_repository.dart';
import 'domain/repositories/get_rating_repository.dart';
import 'domain/repositories/put_rating_repository.dart';
import 'domain/repositories/set_comment_repository.dart';
import 'domain/repositories/set_rating_repository.dart';
import 'domain/usecases/get_comments_usecase.dart';
import 'domain/usecases/get_pokemon_description_usecase.dart';
import 'domain/usecases/get_pokemon_usecase.dart';
import 'domain/usecases/get_rating_usecase.dart';
import 'domain/usecases/put_rating_usecase.dart';
import 'domain/usecases/set_comment_usecase.dart';
import 'domain/usecases/set_rating_usecase.dart';
import 'external/datasources/get_comments_datasource_impl.dart';
import 'external/datasources/get_pokemon_datasource_impl.dart';
import 'external/datasources/get_pokemon_description_datasource_impl.dart';
import 'external/datasources/get_rating_datasource_impl.dart';
import 'external/datasources/put_rating_datasource_impl.dart';
import 'external/datasources/set_comment_datasource_impl.dart';
import 'external/datasources/set_rating_datasource_impl.dart';
import 'infra/datasources/get_comments_datasource.dart';
import 'infra/datasources/get_pokemon_datasource.dart';
import 'infra/datasources/get_pokemon_description_datasource.dart';
import 'infra/datasources/get_rating_datasource.dart';
import 'infra/datasources/put_rating_datasource.dart';
import 'infra/datasources/set_comment_datasource.dart';
import 'infra/datasources/set_rating_datasource.dart';
import 'infra/repositories/get_comments_repository_impl.dart';
import 'infra/repositories/get_pokemon_description_repository_impl.dart';
import 'infra/repositories/get_pokemon_repository_impl.dart';
import 'infra/repositories/get_rating_repository_impl.dart';
import 'infra/repositories/put_rating_repository_impl.dart';
import 'infra/repositories/set_comment_repository_impl.dart';
import 'infra/repositories/set_rating_repository_impl.dart';
import 'infra/usecases/get_comments_usecase_impl.dart';
import 'infra/usecases/get_pokemon_description_usecase_impl.dart';
import 'infra/usecases/get_pokemon_usecase_impl.dart';
import 'infra/usecases/get_rating_usecase_impl.dart';
import 'infra/usecases/put_rating_usecase_impl.dart';
import 'infra/usecases/send_comment_usecase_impl.dart';
import 'infra/usecases/set_rating_usecase_impl.dart';
import 'presenter/controllers/pokemon_controller.dart';
import 'presenter/pages/pokemon_page.dart';
import 'package:http/http.dart';

class PokemonModule extends Module {
  @override
  List<Bind<Object>> get binds => [
        // Others
        Bind.factory((i) => CustomNavigator()),
        // services
        Bind.factory((i) => DBService()),
        Bind.factory((i) => Client()),

        // UseCases
        Bind.factory(
          (i) => GetCommentsUsecaseImpl(
            i.get<GetCommentsRepository>(),
          ),
        ),
        Bind.factory(
          (i) => SetCommentUsecaseImpl(
            i.get<SetCommentRepository>(),
          ),
        ),
        Bind.factory(
          (i) => PutRatingUsecaseImpl(
            i.get<PutRatingRepository>(),
          ),
        ),
        Bind.factory(
          (i) => SetRatingUsecaseImpl(
            i.get<SetRatingRepository>(),
          ),
        ),
        Bind.factory(
          (i) => GetRatingUsecaseImpl(
            i.get<GetRatingRepository>(),
          ),
        ),
        Bind.factory(
          (i) => GetPokemonUsecaseImpl(
            i.get<GetPokemonRepository>(),
          ),
        ),
        Bind.factory(
          (i) => GetPokemonDescriptionUsecaseImpl(
            i.get<GetPokemonDescriptionRepository>(),
          ),
        ),
        // Repositories
        Bind.factory(
          (i) => GetCommentsRepositoryImpl(
            i.get<GetCommentsDataSource>(),
          ),
        ),
        Bind.factory(
          (i) => SetCommentRepositoryImpl(
            i.get<SetCommentDataSource>(),
          ),
        ),
        Bind.factory(
          (i) => PutRatingRepositoryImpl(
            i.get<PutRatingDataSource>(),
          ),
        ),
        Bind.factory(
          (i) => SetRatingRepositoryImpl(
            i.get<SetRatingDataSource>(),
          ),
        ),
        Bind.factory(
          (i) => GetRatingRepositoryImpl(
            i.get<GetRatingDataSource>(),
          ),
        ),
        Bind.factory(
          (i) => GetPokemonRepositoryImpl(
            i.get<GetPokemonDataSource>(),
          ),
        ),
        Bind.factory(
          (i) => GetPokemonDescriptionRepositoryImpl(
            i.get<GetPokemonDescriptionDataSource>(),
          ),
        ),
        // Datasources
        Bind.factory(
          (i) => GetCommentsDataSourceImpl(
            i.get<DBService>(),
          ),
        ),
        Bind.factory(
          (i) => SetCommentDataSourceImpl(
            i.get<DBService>(),
          ),
        ),
        Bind.factory(
          (i) => PutRatingDataSourceImpl(
            i.get<DBService>(),
          ),
        ),
        Bind.factory(
          (i) => SetRatingDataSourceImpl(
            i.get<DBService>(),
          ),
        ),
        Bind.factory(
          (i) => GetRatingDataSourceImpl(
            i.get<DBService>(),
          ),
        ),
        Bind.factory(
          (i) => GetPokemonDataSourceImpl(i.get<Client>()),
        ),
        Bind.factory(
          (i) => GetPokemonDescriptionDataSourceImpl(i.get<Client>()),
        ),
        // Controllers
        Bind.factory(
          (i) => PokemonController(
            i.get<GetPokemonUsecase>(),
            i.get<GetPokemonDescriptionUsecase>(),
            i.get<GetRatingUsecase>(),
            i.get<SetRatingUsecase>(),
            i.get<PutRatingUsecase>(),
            i.get<SetCommentUsecase>(),
            i.get<GetCommentsUsecase>(),
            i.get<CustomNavigator>(),
          ),
        ),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(
          Routes.pokemon.path,
          child: (context, args) => PokemonPage(
            controller: context.read<PokemonController>(),
            pokemonPageInfo: args.data as PokemonPageEntity,
          ),
          transition: TransitionType.rightToLeft,
        ),
      ];
}
