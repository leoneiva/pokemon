import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:pokemon/app/core/constants/poke_colors.dart';

import '../../../../core/helpers/spaces_helper.dart';
import '../../../../core/presenter/widgets/poke_loader.dart';
import '../../domain/entities/pokemon_page_entity.dart';
import '../controllers/pokemon_controller.dart';
import '../widgets/pokemon_about_widget.dart';
import '../widgets/pokemon_comments_widget.dart';
import '../widgets/pokemon_description_widget.dart';

class PokemonPage extends StatefulWidget {
  final PokemonPageEntity pokemonPageInfo;
  final PokemonController controller;

  const PokemonPage({
    Key? key,
    required this.pokemonPageInfo,
    required this.controller,
  }) : super(key: key);

  @override
  State<PokemonPage> createState() => _PokemonPageState();
}

class _PokemonPageState extends State<PokemonPage> {
  @override
  void initState() {
    super.initState();
    widget.controller.getPokemon(idPokemon: widget.pokemonPageInfo.id);
    widget.controller
        .getPokemonDescription(idPokemon: widget.pokemonPageInfo.id);
    widget.controller.getComments(idPokemon: widget.pokemonPageInfo.id);
    widget.controller.getRating(idPokemon: widget.pokemonPageInfo.id);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: PokeColors.primaryColor,
        body: Stack(
          children: [
            Positioned(
              top: 30,
              right: 40,
              child: Image.asset(
                'assets/images/bg_pokeball.png',
                width: MediaQuery.of(context).size.width * 0.5,
                height: MediaQuery.of(context).size.width * 0.5,
              ),
            ),
            Observer(
              builder: (_) => Column(
                children: [
                  AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(
                      widget.controller.isLoading
                          ? ''
                          : widget.pokemonPageInfo.name,
                      style: const TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  widget.controller.isLoading
                      ? Expanded(
                          child: PokeLoader.loading(color: PokeColors.white),
                        )
                      : Expanded(
                          child: ListView(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Image.network(
                                    widget.pokemonPageInfo.image,
                                    width:
                                        MediaQuery.of(context).size.width * 0.5,
                                    height:
                                        MediaQuery.of(context).size.width * 0.5,
                                  )
                                ],
                              ),
                              Container(
                                margin: const EdgeInsets.all(4.0),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20.0),
                                decoration: BoxDecoration(
                                  color: PokeColors.white,
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                child: Column(
                                  children: [
                                    SpacesHelper.h(40.0),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: widget.controller.getTypes(),
                                    ),
                                    SpacesHelper.h(28.0),
                                    PokemonAboutWidget(
                                      height:
                                          widget.controller.pokemon.height ?? 0,
                                      weight:
                                          widget.controller.pokemon.weight ?? 0,
                                      moves: widget.controller.getMoves(),
                                    ),
                                    SpacesHelper.h(20.0),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        widget.controller.showDescription
                                            ? Flexible(
                                                child: Text(
                                                  widget.controller
                                                      .getDescription(),
                                                  textAlign: TextAlign.justify,
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w400,
                                                    color: PokeColors.dark,
                                                  ),
                                                ),
                                              )
                                            : Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 20.0, bottom: 20.0),
                                                child: PokeLoader.loading(),
                                              ),
                                      ],
                                    ),
                                    SpacesHelper.h(20.0),
                                    PokemonStatusWidget(
                                        controller: widget.controller),
                                    SpacesHelper.h(28.0),
                                    PokemonCommentsWidget(
                                      isLoading:
                                          widget.controller.showCommentsRating,
                                      comments:
                                          widget.controller.getPokemonComments,
                                      rating: widget.controller.rating,
                                      onTapRating: (int rating) {
                                        if (widget.controller.ratingPokemon
                                                .rating! ==
                                            0) {
                                          widget.controller.setRating(
                                            idPokemon:
                                                widget.pokemonPageInfo.id,
                                            rating: rating,
                                          );
                                        } else {
                                          widget.controller.putRating(
                                            idPokemon:
                                                widget.pokemonPageInfo.id,
                                            rating: rating,
                                          );
                                        }
                                      },
                                      onTapBtnComment: (String comment) =>
                                          widget.controller.setComment(
                                        idPokemon: widget.pokemonPageInfo.id,
                                        comment: comment,
                                      ),
                                    ),
                                    SpacesHelper.h(20.0),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
