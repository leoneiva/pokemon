import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:asuka/asuka.dart' as asuka;
import 'package:pokemon/app/core/utils/custom_navigator.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/send_rating_entity.dart';

import '../../../../core/constants/poke_colors.dart';
import '../../../../core/domain/errors/errors.dart';
import '../../domain/entities/comment_entity.dart';
import '../../domain/usecases/get_comments_usecase.dart';
import '../../domain/entities/pokemon_entity.dart';
import '../../domain/entities/rating_entity.dart';
import '../../domain/entities/send_comment_entity.dart';
import '../../domain/usecases/get_pokemon_description_usecase.dart';
import '../../domain/usecases/get_pokemon_usecase.dart';
import '../../domain/usecases/get_rating_usecase.dart';
import '../../domain/usecases/put_rating_usecase.dart';
import '../../domain/usecases/set_comment_usecase.dart';
import '../../domain/usecases/set_rating_usecase.dart';
import '../widgets/poke_chip_type_widget.dart';

part 'pokemon_controller.g.dart';

class PokemonController = PokemonControllerBase with _$PokemonController;

abstract class PokemonControllerBase with Store {
  final GetPokemonUsecase _getPokemonUsecase;
  final GetPokemonDescriptionUsecase _getPokemonDescriptionUsecase;
  final GetRatingUsecase _getRatingUsecase;
  final SetRatingUsecase _setRatingUsecase;
  final PutRatingUsecase _putRatingUsecase;
  final SetCommentUsecase _setCommentUsecase;
  final GetCommentsUsecase _getCommentsUsecase;
  final CustomNavigator _customNavigator;

  PokemonControllerBase(
    this._getPokemonUsecase,
    this._getPokemonDescriptionUsecase,
    this._getRatingUsecase,
    this._setRatingUsecase,
    this._putRatingUsecase,
    this._setCommentUsecase,
    this._getCommentsUsecase,
    this._customNavigator,
  );

  @observable
  bool isLoading = true;

  @observable
  bool isLoadingDescription = false;

  @observable
  bool isLoadingComments = true;

  @observable
  bool isLoadingRating = true;

  @observable
  PokemonEntity pokemon = PokemonEntity();

  @observable
  List<CommentEntity> comments = [];

  @computed
  List<CommentEntity> get getPokemonComments => comments;

  @observable
  RatingEntity ratingPokemon = RatingEntity(rating: 0);

  @observable
  int rating = 0;

  @computed
  bool get showDescription => !isLoading && !isLoadingDescription;

  @computed
  bool get showCommentsRating => isLoadingComments || isLoadingRating;

  String getStatus(int index) {
    if (pokemon.stats != null) {
      return pokemon.stats![index].baseStat! < 100
          ? pokemon.stats![index].baseStat!.toString()
          : '100';
    }
    return '';
  }

  List<Widget> getTypes() {
    List<PokeChipTypeWidget> types = [];
    if (pokemon.types != null) {
      for (var t in pokemon.types!) {
        types.add(PokeChipTypeWidget(name: t.type!.name!));
      }
    }
    return types;
  }

  List<Widget> getMoves() {
    List<Text> moves = [];
    if (pokemon.moves != null) {
      for (var i = 0; i <= 1; i++) {
        moves.add(
          Text(
            pokemon.moves![i].move!.name!,
            maxLines: 2,
            style: TextStyle(
              fontSize: 10,
              fontWeight: FontWeight.w400,
              color: PokeColors.dark,
            ),
          ),
        );
      }
    }
    return moves;
  }

  String getDescription() {
    if (pokemon.description != null) {
      return pokemon.description!;
    }
    return '';
  }

  double getProgressBar(int index) {
    if (pokemon.stats != null) {
      return double.parse(pokemon.stats![index].baseStat! < 100
          ? '0.${pokemon.stats![index].baseStat!}'
          : '0.999');
    }
    return 0.0;
  }

  @action
  Future<void> getPokemon({required int idPokemon}) async {
    isLoading = true;
    try {
      final result = await _getPokemonUsecase(
        id: idPokemon,
      );
      result.fold(
        (f) async {
          if (f is NotFoundError) {
            asuka.AsukaSnackbar.message(f.message!).show();
            _customNavigator.pop();
          } else if (f is TimeOutError) {
            asuka.AsukaSnackbar.message(f.message!).show();
          } else if (f is InternalError) {
            asuka.AsukaSnackbar.message(f.message!).show();
          }
        },
        (s) => pokemon = s,
      );
      isLoading = false;
    } catch (error) {
      isLoading = false;
      rethrow;
    }
  }

  @action
  Future<void> getPokemonDescription({required int idPokemon}) async {
    isLoadingDescription = true;
    try {
      final result = await _getPokemonDescriptionUsecase(
        id: idPokemon,
      );
      result.fold(
        (f) async {
          if (f is NotFoundError) {
            asuka.AsukaSnackbar.message(f.message!).show();
          } else if (f is TimeOutError) {
            asuka.AsukaSnackbar.message(f.message!).show();
          } else if (f is InternalError) {
            asuka.AsukaSnackbar.message(f.message!).show();
          }
        },
        (s) => pokemon.description = s,
      );
      isLoadingDescription = false;
    } catch (error) {
      isLoadingDescription = false;
    }
  }

  @action
  Future<void> getComments({required int idPokemon}) async {
    var result = await _getCommentsUsecase(id: idPokemon);
    result.fold(
      (f) async {
        if (f is NotFoundError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        } else if (f is TimeOutError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        } else if (f is InternalError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        }
      },
      (s) {
        comments = s.reversed.toList();
      },
    );
  }

  @action
  Future<void> setComment(
      {required int idPokemon, required String comment}) async {
    var result = await _setCommentUsecase(
      comment: SendCommentEntity(pokemonId: idPokemon, comment: comment),
    );
    result.fold(
      (f) async {
        if (f is NotFoundError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        } else if (f is TimeOutError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        } else if (f is InternalError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        }
      },
      (s) => getComments(idPokemon: idPokemon),
    );
  }

  @action
  Future<void> getRating({required int idPokemon}) async {
    var result = await _getRatingUsecase(id: idPokemon);
    result.fold(
      (f) async {
        if (f is NotFoundError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        } else if (f is TimeOutError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        } else if (f is InternalError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        }
      },
      (s) {
        ratingPokemon = s;
        rating = s.rating ?? 0;
      },
    );
  }

  @action
  Future<void> setRating({required int idPokemon, required int rating}) async {
    var result = await _setRatingUsecase(
      rating: SendRatingEntity(pokemonId: idPokemon, rating: rating),
    );
    result.fold(
      (f) async {
        if (f is NotFoundError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        } else if (f is TimeOutError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        } else if (f is InternalError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        }
      },
      (s) => getRating(idPokemon: idPokemon),
    );
  }

  @action
  Future<void> putRating({required int idPokemon, required int rating}) async {
    var result = await _putRatingUsecase(
      rating: SendRatingEntity(
        id: ratingPokemon.id,
        rating: rating,
        pokemonId: idPokemon,
      ),
    );
    result.fold(
      (f) async {
        if (f is NotFoundError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        } else if (f is TimeOutError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        } else if (f is InternalError) {
          asuka.AsukaSnackbar.message(f.message!).show();
        }
      },
      (s) => getRating(idPokemon: idPokemon),
    );
  }
}
