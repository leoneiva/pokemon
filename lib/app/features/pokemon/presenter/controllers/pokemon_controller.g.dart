// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$PokemonController on PokemonControllerBase, Store {
  Computed<List<CommentEntity>>? _$getPokemonCommentsComputed;

  @override
  List<CommentEntity> get getPokemonComments =>
      (_$getPokemonCommentsComputed ??= Computed<List<CommentEntity>>(
              () => super.getPokemonComments,
              name: 'PokemonControllerBase.getPokemonComments'))
          .value;
  Computed<bool>? _$showDescriptionComputed;

  @override
  bool get showDescription =>
      (_$showDescriptionComputed ??= Computed<bool>(() => super.showDescription,
              name: 'PokemonControllerBase.showDescription'))
          .value;
  Computed<bool>? _$showCommentsRatingComputed;

  @override
  bool get showCommentsRating => (_$showCommentsRatingComputed ??=
          Computed<bool>(() => super.showCommentsRating,
              name: 'PokemonControllerBase.showCommentsRating'))
      .value;

  late final _$isLoadingAtom =
      Atom(name: 'PokemonControllerBase.isLoading', context: context);

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  late final _$isLoadingDescriptionAtom = Atom(
      name: 'PokemonControllerBase.isLoadingDescription', context: context);

  @override
  bool get isLoadingDescription {
    _$isLoadingDescriptionAtom.reportRead();
    return super.isLoadingDescription;
  }

  @override
  set isLoadingDescription(bool value) {
    _$isLoadingDescriptionAtom.reportWrite(value, super.isLoadingDescription,
        () {
      super.isLoadingDescription = value;
    });
  }

  late final _$isLoadingCommentsAtom =
      Atom(name: 'PokemonControllerBase.isLoadingComments', context: context);

  @override
  bool get isLoadingComments {
    _$isLoadingCommentsAtom.reportRead();
    return super.isLoadingComments;
  }

  @override
  set isLoadingComments(bool value) {
    _$isLoadingCommentsAtom.reportWrite(value, super.isLoadingComments, () {
      super.isLoadingComments = value;
    });
  }

  late final _$isLoadingRatingAtom =
      Atom(name: 'PokemonControllerBase.isLoadingRating', context: context);

  @override
  bool get isLoadingRating {
    _$isLoadingRatingAtom.reportRead();
    return super.isLoadingRating;
  }

  @override
  set isLoadingRating(bool value) {
    _$isLoadingRatingAtom.reportWrite(value, super.isLoadingRating, () {
      super.isLoadingRating = value;
    });
  }

  late final _$pokemonAtom =
      Atom(name: 'PokemonControllerBase.pokemon', context: context);

  @override
  PokemonEntity get pokemon {
    _$pokemonAtom.reportRead();
    return super.pokemon;
  }

  @override
  set pokemon(PokemonEntity value) {
    _$pokemonAtom.reportWrite(value, super.pokemon, () {
      super.pokemon = value;
    });
  }

  late final _$commentsAtom =
      Atom(name: 'PokemonControllerBase.comments', context: context);

  @override
  List<CommentEntity> get comments {
    _$commentsAtom.reportRead();
    return super.comments;
  }

  @override
  set comments(List<CommentEntity> value) {
    _$commentsAtom.reportWrite(value, super.comments, () {
      super.comments = value;
    });
  }

  late final _$ratingPokemonAtom =
      Atom(name: 'PokemonControllerBase.ratingPokemon', context: context);

  @override
  RatingEntity get ratingPokemon {
    _$ratingPokemonAtom.reportRead();
    return super.ratingPokemon;
  }

  @override
  set ratingPokemon(RatingEntity value) {
    _$ratingPokemonAtom.reportWrite(value, super.ratingPokemon, () {
      super.ratingPokemon = value;
    });
  }

  late final _$ratingAtom =
      Atom(name: 'PokemonControllerBase.rating', context: context);

  @override
  int get rating {
    _$ratingAtom.reportRead();
    return super.rating;
  }

  @override
  set rating(int value) {
    _$ratingAtom.reportWrite(value, super.rating, () {
      super.rating = value;
    });
  }

  late final _$getPokemonAsyncAction =
      AsyncAction('PokemonControllerBase.getPokemon', context: context);

  @override
  Future<void> getPokemon({required int idPokemon}) {
    return _$getPokemonAsyncAction
        .run(() => super.getPokemon(idPokemon: idPokemon));
  }

  late final _$getPokemonDescriptionAsyncAction = AsyncAction(
      'PokemonControllerBase.getPokemonDescription',
      context: context);

  @override
  Future<void> getPokemonDescription({required int idPokemon}) {
    return _$getPokemonDescriptionAsyncAction
        .run(() => super.getPokemonDescription(idPokemon: idPokemon));
  }

  late final _$getCommentsAsyncAction =
      AsyncAction('PokemonControllerBase.getComments', context: context);

  @override
  Future<void> getComments({required int idPokemon}) {
    return _$getCommentsAsyncAction
        .run(() => super.getComments(idPokemon: idPokemon));
  }

  late final _$setCommentAsyncAction =
      AsyncAction('PokemonControllerBase.setComment', context: context);

  @override
  Future<void> setComment({required int idPokemon, required String comment}) {
    return _$setCommentAsyncAction
        .run(() => super.setComment(idPokemon: idPokemon, comment: comment));
  }

  late final _$getRatingAsyncAction =
      AsyncAction('PokemonControllerBase.getRating', context: context);

  @override
  Future<void> getRating({required int idPokemon}) {
    return _$getRatingAsyncAction
        .run(() => super.getRating(idPokemon: idPokemon));
  }

  late final _$setRatingAsyncAction =
      AsyncAction('PokemonControllerBase.setRating', context: context);

  @override
  Future<void> setRating({required int idPokemon, required int rating}) {
    return _$setRatingAsyncAction
        .run(() => super.setRating(idPokemon: idPokemon, rating: rating));
  }

  late final _$putRatingAsyncAction =
      AsyncAction('PokemonControllerBase.putRating', context: context);

  @override
  Future<void> putRating({required int idPokemon, required int rating}) {
    return _$putRatingAsyncAction
        .run(() => super.putRating(idPokemon: idPokemon, rating: rating));
  }

  @override
  String toString() {
    return '''
isLoading: ${isLoading},
isLoadingDescription: ${isLoadingDescription},
isLoadingComments: ${isLoadingComments},
isLoadingRating: ${isLoadingRating},
pokemon: ${pokemon},
comments: ${comments},
ratingPokemon: ${ratingPokemon},
rating: ${rating},
getPokemonComments: ${getPokemonComments},
showDescription: ${showDescription},
showCommentsRating: ${showCommentsRating}
    ''';
  }
}
