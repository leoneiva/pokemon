import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../../../../core/constants/poke_colors.dart';
import '../../../../core/helpers/spaces_helper.dart';
import '../controllers/pokemon_controller.dart';

class PokemonStatusWidget extends StatelessWidget {
  const PokemonStatusWidget({
    super.key,
    required this.controller,
  });

  final PokemonController controller;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Base Stats',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: PokeColors.primaryColor,
              ),
            )
          ],
        ),
        SpacesHelper.h(20.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  'HP',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w700,
                    color: PokeColors.primaryColor,
                  ),
                ),
                SpacesHelper.h(10.0),
                Text(
                  'ATK',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w700,
                    color: PokeColors.primaryColor,
                  ),
                ),
                SpacesHelper.h(10.0),
                Text(
                  'DEF',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w700,
                    color: PokeColors.primaryColor,
                  ),
                ),
                SpacesHelper.h(10.0),
                Text(
                  'SATK',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w700,
                    color: PokeColors.primaryColor,
                  ),
                ),
                SpacesHelper.h(10.0),
                Text(
                  'SDEF',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w700,
                    color: PokeColors.primaryColor,
                  ),
                ),
                SpacesHelper.h(10.0),
                Text(
                  'SPD',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w700,
                    color: PokeColors.primaryColor,
                  ),
                ),
              ],
            ),
            SpacesHelper.w(12.0),
            Container(
              height: 130,
              decoration: BoxDecoration(
                border: Border(
                  right: BorderSide(
                    width: 1.5,
                    color: PokeColors.primaryColor,
                  ),
                ),
              ),
            ),
            SpacesHelper.w(12.0),
            Column(
              children: [
                Row(
                  children: [
                    Text(
                      controller.getStatus(0),
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w400,
                        color: PokeColors.dark,
                      ),
                    ),
                    SpacesHelper.w(8.0),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width * 0.69,
                      lineHeight: 4.0,
                      percent: controller.getProgressBar(0),
                      progressColor: PokeColors.primaryColor,
                    ),
                  ],
                ),
                SpacesHelper.h(10.0),
                Row(
                  children: [
                    Text(
                      controller.getStatus(1),
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w400,
                        color: PokeColors.dark,
                      ),
                    ),
                    SpacesHelper.w(8.0),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width * 0.69,
                      lineHeight: 4.0,
                      percent: controller.getProgressBar(1),
                      progressColor: PokeColors.primaryColor,
                    ),
                  ],
                ),
                SpacesHelper.h(10.0),
                Row(
                  children: [
                    Text(
                      controller.getStatus(2),
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w400,
                        color: PokeColors.dark,
                      ),
                    ),
                    SpacesHelper.w(8.0),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width * 0.69,
                      lineHeight: 4.0,
                      percent: controller.getProgressBar(2),
                      progressColor: PokeColors.primaryColor,
                    ),
                  ],
                ),
                SpacesHelper.h(10.0),
                Row(
                  children: [
                    Text(
                      controller.getStatus(3),
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w400,
                        color: PokeColors.dark,
                      ),
                    ),
                    SpacesHelper.w(8.0),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width * 0.69,
                      lineHeight: 4.0,
                      percent: controller.getProgressBar(3),
                      progressColor: PokeColors.primaryColor,
                    ),
                  ],
                ),
                SpacesHelper.h(10.0),
                Row(
                  children: [
                    Text(
                      controller.getStatus(4),
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w400,
                        color: PokeColors.dark,
                      ),
                    ),
                    SpacesHelper.w(8.0),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width * 0.69,
                      lineHeight: 4.0,
                      percent: controller.getProgressBar(4),
                      progressColor: PokeColors.primaryColor,
                    ),
                  ],
                ),
                SpacesHelper.h(10.0),
                Row(
                  children: [
                    Text(
                      controller.getStatus(5),
                      style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w400,
                        color: PokeColors.dark,
                      ),
                    ),
                    SpacesHelper.w(8.0),
                    LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width * 0.69,
                      lineHeight: 4.0,
                      percent: controller.getProgressBar(5),
                      progressColor: PokeColors.primaryColor,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
