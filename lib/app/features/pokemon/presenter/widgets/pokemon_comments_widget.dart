// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

import '../../../../core/constants/poke_colors.dart';
import '../../../../core/helpers/spaces_helper.dart';
import '../../domain/entities/comment_entity.dart';

class PokemonCommentsWidget extends StatefulWidget {
  int rating;
  List<CommentEntity> comments;
  bool isLoading;
  Function onTapRating;
  Function onTapBtnComment;

  PokemonCommentsWidget({
    Key? key,
    required this.rating,
    required this.comments,
    required this.isLoading,
    required this.onTapRating,
    required this.onTapBtnComment,
  }) : super(key: key);

  @override
  State<PokemonCommentsWidget> createState() => _PokemonCommentsWidgetState();
}

class _PokemonCommentsWidgetState extends State<PokemonCommentsWidget> {
  TextEditingController cComment = TextEditingController();
  bool enableButton = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'how much do you like this pokémon?',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: PokeColors.primaryColor,
              ),
            )
          ],
        ),
        SpacesHelper.h(8.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () => widget.onTapRating(1),
              child: Icon(
                widget.rating >= 1
                    ? Icons.star_rate_rounded
                    : Icons.star_border_rounded,
                color: widget.rating >= 1 ? Colors.yellow : Colors.black45,
                size: 28.0,
              ),
            ),
            GestureDetector(
              onTap: () => widget.onTapRating(2),
              child: Icon(
                widget.rating >= 2
                    ? Icons.star_rate_rounded
                    : Icons.star_border_rounded,
                color: widget.rating >= 2 ? Colors.yellow : Colors.black45,
                size: 28.0,
              ),
            ),
            GestureDetector(
              onTap: () => widget.onTapRating(3),
              child: Icon(
                widget.rating >= 3
                    ? Icons.star_rate_rounded
                    : Icons.star_border_rounded,
                color: widget.rating >= 3 ? Colors.yellow : Colors.black45,
                size: 28.0,
              ),
            ),
            GestureDetector(
              onTap: () => widget.onTapRating(4),
              child: Icon(
                widget.rating >= 4
                    ? Icons.star_rate_rounded
                    : Icons.star_border_rounded,
                color: widget.rating >= 4 ? Colors.yellow : Colors.black45,
                size: 28.0,
              ),
            ),
            GestureDetector(
              onTap: () => widget.onTapRating(5),
              child: Icon(
                widget.rating == 5
                    ? Icons.star_rate_rounded
                    : Icons.star_border_rounded,
                color: widget.rating == 5 ? Colors.yellow : Colors.black45,
                size: 28.0,
              ),
            ),
          ],
        ),
        SpacesHelper.h(10.0),
        Form(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.4,
                    height: 35,
                    child: TextField(
                      controller: cComment,
                      style: const TextStyle(
                        fontSize: 12,
                      ),
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.0),
                          ),
                        ),
                        contentPadding: EdgeInsets.all(10),
                        hintText: 'comment',
                        hintStyle: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                      textInputAction: TextInputAction.done,
                      onChanged: (String value) {
                        if (value.isNotEmpty) {
                          setState(() {
                            enableButton = true;
                          });
                        } else {
                          setState(() {
                            enableButton = false;
                          });
                        }
                      },
                    ),
                  ),
                ],
              ),
              SpacesHelper.h(10.0),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.4,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: 45,
                      height: 26,
                      child: TextButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                            enableButton
                                ? PokeColors.primaryColor
                                : PokeColors.disableColor,
                          ),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                          ),
                        ),
                        onPressed: enableButton
                            ? () {
                                widget.onTapBtnComment(cComment.text);
                                cComment.text = '';
                              }
                            : () {},
                        child: Text(
                          'send',
                          style: TextStyle(
                            color: PokeColors.white,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        SpacesHelper.h(25.0),
        ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: widget.comments.length,
          itemBuilder: (context, position) {
            return Container(
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                color: PokeColors.primaryColor,
                borderRadius: BorderRadius.circular(8),
              ),
              child: Row(
                children: [
                  Icon(
                    Icons.comment,
                    color: PokeColors.white,
                  ),
                  SpacesHelper.w(10.0),
                  Text(
                    widget.comments[position].comment ?? '',
                    style: TextStyle(
                      color: PokeColors.white,
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ],
    );
  }
}
