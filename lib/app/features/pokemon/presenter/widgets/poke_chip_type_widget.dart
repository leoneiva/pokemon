import 'package:flutter/material.dart';

import '../../../../core/constants/poke_colors.dart';

class PokeChipTypeWidget extends StatelessWidget {
  const PokeChipTypeWidget({
    super.key,
    required this.name,
  });

  final String name;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        top: 4.0,
        bottom: 4.0,
        left: 16.0,
        right: 16.0,
      ),
      decoration: BoxDecoration(
        color: PokeColors.primaryColor,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Text(
        name,
        style: TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.w700,
          color: PokeColors.white,
        ),
      ),
    );
  }
}
