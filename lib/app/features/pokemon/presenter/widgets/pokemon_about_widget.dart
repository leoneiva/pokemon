import 'package:flutter/material.dart';

import '../../../../core/constants/poke_colors.dart';
import '../../../../core/helpers/spaces_helper.dart';

class PokemonAboutWidget extends StatelessWidget {
  const PokemonAboutWidget({
    super.key,
    required this.moves,
    required this.weight,
    required this.height,
  });

  final List<Widget> moves;
  final int weight;
  final int height;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'About',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: PokeColors.primaryColor,
              ),
            )
          ],
        ),
        SpacesHelper.h(28.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Text(
                  '$weight kg',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w400,
                    color: PokeColors.dark,
                  ),
                ),
                SpacesHelper.h(10.0),
                Text(
                  'Weight',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w400,
                    color: PokeColors.dark,
                  ),
                ),
              ],
            ),
            Container(
              height: 48,
              decoration: BoxDecoration(
                border: Border(
                  right: BorderSide(
                    width: 1.5,
                    color: PokeColors.primaryColor,
                  ),
                ),
              ),
            ),
            Column(
              children: [
                Text(
                  '$height m',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w400,
                    color: PokeColors.dark,
                  ),
                ),
                SpacesHelper.h(10.0),
                Text(
                  'Height',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w400,
                    color: PokeColors.dark,
                  ),
                ),
              ],
            ),
            Container(
              height: 48,
              decoration: BoxDecoration(
                border: Border(
                  right: BorderSide(
                    width: 1.5,
                    color: PokeColors.primaryColor,
                  ),
                ),
              ),
            ),
            Column(
              children: [
                Column(
                  children: moves,
                ),
                SpacesHelper.h(10.0),
                Text(
                  'Moves',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w400,
                    color: PokeColors.dark,
                  ),
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
