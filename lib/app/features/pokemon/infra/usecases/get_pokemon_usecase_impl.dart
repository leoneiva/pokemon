import '../../domain/entities/pokemon_entity.dart';
import '../../domain/repositories/get_pokemon_repository.dart';
import '../../domain/usecases/get_pokemon_usecase.dart';
import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';

class GetPokemonUsecaseImpl extends GetPokemonUsecase {
  final GetPokemonRepository _repository;
  GetPokemonUsecaseImpl(this._repository);

  @override
  Future<Either<Failure, PokemonEntity>> call({
    required int id,
  }) async {
    return _repository.getPokemon(id: id);
  }
}
