import '../../domain/entities/rating_entity.dart';
import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../../domain/repositories/get_rating_repository.dart';
import '../../domain/usecases/get_rating_usecase.dart';

class GetRatingUsecaseImpl extends GetRatingUsecase {
  final GetRatingRepository _repository;
  GetRatingUsecaseImpl(this._repository);

  @override
  Future<Either<Failure, RatingEntity>> call({
    required int id,
  }) async {
    return _repository.getRating(id: id);
  }
}
