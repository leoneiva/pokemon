import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../../domain/entities/comment_entity.dart';
import '../../domain/repositories/get_comments_repository.dart';
import '../../domain/usecases/get_comments_usecase.dart';

class GetCommentsUsecaseImpl extends GetCommentsUsecase {
  final GetCommentsRepository _repository;
  GetCommentsUsecaseImpl(this._repository);

  @override
  Future<Either<Failure, List<CommentEntity>>> call({
    required int id,
  }) async {
    return _repository.getComments(id: id);
  }
}
