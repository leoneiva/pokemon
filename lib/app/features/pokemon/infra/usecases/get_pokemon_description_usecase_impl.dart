import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../../domain/repositories/get_pokemon_description_repository.dart';
import '../../domain/usecases/get_pokemon_description_usecase.dart';

class GetPokemonDescriptionUsecaseImpl extends GetPokemonDescriptionUsecase {
  final GetPokemonDescriptionRepository _repository;
  GetPokemonDescriptionUsecaseImpl(this._repository);

  @override
  Future<Either<Failure, String>> call({
    required int id,
  }) async {
    return _repository.getPokemonDescription(id: id);
  }
}
