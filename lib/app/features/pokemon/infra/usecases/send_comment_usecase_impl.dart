import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../../domain/entities/send_comment_entity.dart';
import '../../domain/repositories/set_comment_repository.dart';
import '../../domain/usecases/set_comment_usecase.dart';

class SetCommentUsecaseImpl extends SetCommentUsecase {
  final SetCommentRepository _repository;
  SetCommentUsecaseImpl(this._repository);

  @override
  Future<Either<Failure, void>> call({
    required SendCommentEntity comment,
  }) async {
    return _repository.setComment(comment: comment);
  }
}
