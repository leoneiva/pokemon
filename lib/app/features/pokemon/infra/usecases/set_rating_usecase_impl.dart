import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../../domain/entities/send_rating_entity.dart';
import '../../domain/repositories/set_rating_repository.dart';
import '../../domain/usecases/set_rating_usecase.dart';

class SetRatingUsecaseImpl extends SetRatingUsecase {
  final SetRatingRepository _repository;
  SetRatingUsecaseImpl(this._repository);

  @override
  Future<Either<Failure, void>> call({
    required SendRatingEntity rating,
  }) async {
    return _repository.setRating(rating: rating);
  }
}
