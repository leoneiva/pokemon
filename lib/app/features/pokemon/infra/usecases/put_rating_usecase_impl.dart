import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../../domain/entities/send_rating_entity.dart';
import '../../domain/repositories/put_rating_repository.dart';
import '../../domain/usecases/put_rating_usecase.dart';

class PutRatingUsecaseImpl extends PutRatingUsecase {
  final PutRatingRepository _repository;
  PutRatingUsecaseImpl(this._repository);

  @override
  Future<Either<Failure, void>> call({
    required SendRatingEntity rating,
  }) async {
    return _repository.putRating(rating: rating);
  }
}
