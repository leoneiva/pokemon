import 'package:dartz/dartz.dart';
import 'package:pokemon/app/core/rest_client/rest_client_erros_const.dart';
import 'package:pokemon/app/core/rest_client/rest_client_exception.dart';
import '../../../../core/domain/errors/errors.dart';
import '../../domain/entities/send_rating_entity.dart';
import '../../domain/repositories/put_rating_repository.dart';
import '../datasources/put_rating_datasource.dart';

class PutRatingRepositoryImpl implements PutRatingRepository {
  final PutRatingDataSource _dataSource;

  PutRatingRepositoryImpl(this._dataSource);

  @override
  Future<Either<Failure, void>> putRating({
    required SendRatingEntity rating,
  }) async {
    try {
      final result = await _dataSource.putRating(rating: rating);
      return Right(result);
    } on RestClientException catch (e) {
      final code = e.statusCode;
      final message = e.message;

      switch (code) {
        case RestClientErrors.internalServerError:
          return Left(InternalError(message: message));
        case RestClientErrors.timeOutError:
          return Left(TimeOutError(message: message));
        case RestClientErrors.notFoundError:
          return Left(NotFoundError(message: message));
        default:
          rethrow;
      }
    }
  }
}
