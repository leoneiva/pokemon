import '../../../../core/rest_client/rest_client_erros_const.dart';
import '../../../../core/rest_client/rest_client_exception.dart';
import '../../domain/entities/pokemon_entity.dart';
import '../../domain/repositories/get_pokemon_repository.dart';
import '../datasources/get_pokemon_datasource.dart';
import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';

class GetPokemonRepositoryImpl implements GetPokemonRepository {
  final GetPokemonDataSource _dataSource;

  GetPokemonRepositoryImpl(this._dataSource);

  @override
  Future<Either<Failure, PokemonEntity>> getPokemon({
    required int id,
  }) async {
    try {
      final result = await _dataSource.getPokemon(id: id);
      return Right(result);
    } on RestClientException catch (e) {
      final code = e.statusCode;
      final message = e.message;

      switch (code) {
        case RestClientErrors.internalServerError:
          return Left(InternalError(message: message));
        case RestClientErrors.timeOutError:
          return Left(TimeOutError(message: message));
        case RestClientErrors.notFoundError:
          return Left(NotFoundError(message: message));
        default:
          rethrow;
      }
    }
  }
}
