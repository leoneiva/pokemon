import 'package:dartz/dartz.dart';
import 'package:pokemon/app/core/rest_client/rest_client_erros_const.dart';
import 'package:pokemon/app/core/rest_client/rest_client_exception.dart';
import '../../../../core/domain/errors/errors.dart';
import '../../domain/entities/comment_entity.dart';
import '../../domain/repositories/get_comments_repository.dart';
import '../datasources/get_comments_datasource.dart';

class GetCommentsRepositoryImpl implements GetCommentsRepository {
  final GetCommentsDataSource _dataSource;

  GetCommentsRepositoryImpl(this._dataSource);

  @override
  Future<Either<Failure, List<CommentEntity>>> getComments({
    required int id,
  }) async {
    try {
      final result = await _dataSource.getComments(id: id);
      return Right(result);
    } on RestClientException catch (e) {
      final code = e.statusCode;
      final message = e.message;

      switch (code) {
        case RestClientErrors.internalServerError:
          return Left(InternalError(message: message));
        case RestClientErrors.timeOutError:
          return Left(TimeOutError(message: message));
        case RestClientErrors.notFoundError:
          return Left(NotFoundError(message: message));
        default:
          rethrow;
      }
    }
  }
}
