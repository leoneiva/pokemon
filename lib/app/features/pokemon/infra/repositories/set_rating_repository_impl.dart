import 'package:dartz/dartz.dart';
import 'package:pokemon/app/core/rest_client/rest_client_erros_const.dart';
import 'package:pokemon/app/core/rest_client/rest_client_exception.dart';
import '../../../../core/domain/errors/errors.dart';
import '../../domain/entities/send_rating_entity.dart';
import '../../domain/repositories/set_rating_repository.dart';
import '../datasources/set_rating_datasource.dart';

class SetRatingRepositoryImpl implements SetRatingRepository {
  final SetRatingDataSource _dataSource;

  SetRatingRepositoryImpl(this._dataSource);

  @override
  Future<Either<Failure, void>> setRating({
    required SendRatingEntity rating,
  }) async {
    try {
      final result = await _dataSource.setRating(rating: rating);
      return Right(result);
    } on RestClientException catch (e) {
      final code = e.statusCode;
      final message = e.message;

      switch (code) {
        case RestClientErrors.internalServerError:
          return Left(InternalError(message: message));
        case RestClientErrors.timeOutError:
          return Left(TimeOutError(message: message));
        case RestClientErrors.notFoundError:
          return Left(NotFoundError(message: message));
        default:
          rethrow;
      }
    }
  }
}
