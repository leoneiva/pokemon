import '../../../../core/rest_client/rest_client_erros_const.dart';
import '../../../../core/rest_client/rest_client_exception.dart';
import '../../domain/repositories/get_pokemon_description_repository.dart';
import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../datasources/get_pokemon_description_datasource.dart';

class GetPokemonDescriptionRepositoryImpl
    implements GetPokemonDescriptionRepository {
  final GetPokemonDescriptionDataSource _dataSource;

  GetPokemonDescriptionRepositoryImpl(this._dataSource);

  @override
  Future<Either<Failure, String>> getPokemonDescription({
    required int id,
  }) async {
    try {
      final result = await _dataSource.getPokemonDescription(id: id);
      return Right(result);
    } on RestClientException catch (e) {
      final code = e.statusCode;
      final message = e.message;

      switch (code) {
        case RestClientErrors.internalServerError:
          return Left(InternalError(message: message));
        case RestClientErrors.timeOutError:
          return Left(TimeOutError(message: message));
        case RestClientErrors.notFoundError:
          return Left(NotFoundError(message: message));
        default:
          rethrow;
      }
    }
  }
}
