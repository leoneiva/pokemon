import 'package:dartz/dartz.dart';
import 'package:pokemon/app/core/rest_client/rest_client_erros_const.dart';
import 'package:pokemon/app/core/rest_client/rest_client_exception.dart';
import '../../../../core/domain/errors/errors.dart';
import '../../domain/entities/send_comment_entity.dart';
import '../../domain/repositories/set_comment_repository.dart';
import '../datasources/set_comment_datasource.dart';

class SetCommentRepositoryImpl implements SetCommentRepository {
  final SetCommentDataSource _dataSource;

  SetCommentRepositoryImpl(this._dataSource);

  @override
  Future<Either<Failure, void>> setComment({
    required SendCommentEntity comment,
  }) async {
    try {
      final result = await _dataSource.setComment(comment: comment);
      return Right(result);
    } on RestClientException catch (e) {
      final code = e.statusCode;
      final message = e.message;

      switch (code) {
        case RestClientErrors.internalServerError:
          return Left(InternalError(message: message));
        case RestClientErrors.timeOutError:
          return Left(TimeOutError(message: message));
        case RestClientErrors.notFoundError:
          return Left(NotFoundError(message: message));
        default:
          rethrow;
      }
    }
  }
}
