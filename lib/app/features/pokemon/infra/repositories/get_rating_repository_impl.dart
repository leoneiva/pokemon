import '../../../../core/rest_client/rest_client_erros_const.dart';
import '../../../../core/rest_client/rest_client_exception.dart';
import '../../domain/entities/rating_entity.dart';
import '../../domain/repositories/get_rating_repository.dart';
import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../datasources/get_rating_datasource.dart';

class GetRatingRepositoryImpl implements GetRatingRepository {
  final GetRatingDataSource _dataSource;

  GetRatingRepositoryImpl(this._dataSource);

  @override
  Future<Either<Failure, RatingEntity>> getRating({
    required int id,
  }) async {
    try {
      final result = await _dataSource.getRating(id: id);
      return Right(result);
    } on RestClientException catch (e) {
      final code = e.statusCode;
      final message = e.message;

      switch (code) {
        case RestClientErrors.internalServerError:
          return Left(InternalError(message: message));
        case RestClientErrors.timeOutError:
          return Left(TimeOutError(message: message));
        case RestClientErrors.notFoundError:
          return Left(NotFoundError(message: message));
        default:
          rethrow;
      }
    }
  }
}
