import 'dart:convert';

import '../../domain/entities/pokemon_entity.dart';

class PokemonModel extends PokemonEntity {
  PokemonModel.fromJson(String data) {
    Map<String, dynamic> json = jsonDecode(data);
    if (json['moves'] != null) {
      moves = <MovesModel>[];
      json['moves'].forEach((v) {
        moves!.add(MovesModel.fromJson(v));
      });
    }
    if (json['stats'] != null) {
      stats = <StatsModel>[];
      json['stats'].forEach((v) {
        stats!.add(StatsModel.fromJson(v));
      });
    }
    if (json['types'] != null) {
      types = <TypesModel>[];
      json['types'].forEach((v) {
        types!.add(TypesModel.fromJson(v));
      });
    }
    weight = json['weight'];
    height = json['height'];
  }
}

class MovesModel extends MovesEntity {
  MovesModel.fromJson(Map<String, dynamic> json) {
    move = json['move'] != null ? MoveModel.fromJson(json['move']) : null;
  }
}

class MoveModel extends MoveEntity {
  MoveModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }
}

class StatsModel extends StatsEntity {
  StatsModel.fromJson(Map<String, dynamic> json) {
    baseStat = json['base_stat'];
    effort = json['effort'];
    stat = json['stat'] != null ? MoveModel.fromJson(json['stat']) : null;
  }
}

class TypesModel extends TypesEntity {
  TypesModel.fromJson(Map<String, dynamic> json) {
    slot = json['slot'];
    type = json['type'] != null ? MoveModel.fromJson(json['type']) : null;
  }
}
