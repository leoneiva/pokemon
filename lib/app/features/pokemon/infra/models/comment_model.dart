import '../../domain/entities/comment_entity.dart';

class CommentModel extends CommentEntity {
  CommentModel.fromMap(Map<dynamic, dynamic> map) {
    id = map['id'];
    comment = map['comment'];
    pokemonId = map['pokemonId'];
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'comment': comment,
      'pokemonId': pokemonId,
    };
  }
}
