import 'package:pokemon/app/features/pokemon/domain/entities/rating_entity.dart';

class RatingModel extends RatingEntity {
  RatingModel.fromMap(Map<dynamic, dynamic> map) {
    id = map['id'];
    rating = map['rating'];
    pokemonId = map['pokemonId'];
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'rating': rating,
      'pokemonId': pokemonId,
    };
  }
}
