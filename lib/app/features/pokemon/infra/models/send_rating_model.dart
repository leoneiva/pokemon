class SendRatingModel {
  int? id;
  int? rating;
  int? pokemonId;

  SendRatingModel({this.id, this.rating, this.pokemonId});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'rating': rating,
      'pokemonId': pokemonId,
    };
  }
}
