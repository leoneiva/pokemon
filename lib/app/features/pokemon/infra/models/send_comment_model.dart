class SendCommentModel {
  int? id;
  String? comment;
  int? pokemonId;

  SendCommentModel({this.id, this.comment, this.pokemonId});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'comment': comment,
      'pokemonId': pokemonId,
    };
  }
}
