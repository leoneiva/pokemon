import 'dart:async';

import '../models/rating_model.dart';

abstract class GetRatingDataSource {
  Future<RatingModel> getRating({
    required int id,
  });
}
