import 'dart:async';

import '../models/send_comment_model.dart';

abstract class SetCommentDataSource {
  Future<void> setComment({
    required SendCommentModel comment,
  });
}
