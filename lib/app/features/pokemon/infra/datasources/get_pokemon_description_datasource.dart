import 'dart:async';

abstract class GetPokemonDescriptionDataSource {
  Future<String> getPokemonDescription({
    required int id,
  });
}
