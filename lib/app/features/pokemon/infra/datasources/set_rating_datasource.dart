import 'dart:async';

import '../models/send_rating_model.dart';

abstract class SetRatingDataSource {
  Future<void> setRating({
    required SendRatingModel rating,
  });
}
