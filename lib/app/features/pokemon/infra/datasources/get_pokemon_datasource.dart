import 'dart:async';

import '../models/pokemon_model.dart';

abstract class GetPokemonDataSource {
  Future<PokemonModel> getPokemon({
    required int id,
  });
}
