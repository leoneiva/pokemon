import 'dart:async';

import '../models/send_rating_model.dart';

abstract class PutRatingDataSource {
  Future<void> putRating({
    required SendRatingModel rating,
  });
}
