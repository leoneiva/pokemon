import 'dart:async';

import '../models/comment_model.dart';

abstract class GetCommentsDataSource {
  Future<List<CommentModel>> getComments({
    required int id,
  });
}
