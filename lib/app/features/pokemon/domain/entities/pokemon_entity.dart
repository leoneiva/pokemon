class PokemonEntity {
  List<MovesEntity>? moves;
  List<StatsEntity>? stats;
  List<TypesEntity>? types;
  int? weight;
  int? height;
  String? description;

  PokemonEntity({
    this.moves,
    this.stats,
    this.types,
    this.weight,
    this.height,
    this.description,
  });
}

class MovesEntity {
  MoveEntity? move;

  MovesEntity({this.move});
}

class MoveEntity {
  String? name;
  String? url;

  MoveEntity({this.name, this.url});
}

class StatsEntity {
  int? baseStat;
  int? effort;
  MoveEntity? stat;

  StatsEntity({this.baseStat, this.effort, this.stat});
}

class TypesEntity {
  int? slot;
  MoveEntity? type;

  TypesEntity({this.slot, this.type});
}
