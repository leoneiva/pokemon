class PokemonPageEntity {
  int id;
  String name;
  String image;

  PokemonPageEntity(
      {required this.id, required this.name, required this.image});
}
