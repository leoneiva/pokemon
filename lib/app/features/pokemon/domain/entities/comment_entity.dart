class CommentEntity {
  int? id;
  String? comment;
  int? pokemonId;

  CommentEntity({this.id, this.comment, this.pokemonId});
}
