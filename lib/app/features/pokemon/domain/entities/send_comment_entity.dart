import '../../infra/models/send_comment_model.dart';

class SendCommentEntity extends SendCommentModel {
  int? id;
  String? comment;
  int? pokemonId;

  SendCommentEntity({this.id, this.comment, this.pokemonId});
}
