import 'package:pokemon/app/features/pokemon/infra/models/send_rating_model.dart';

class SendRatingEntity extends SendRatingModel {
  int? id;
  int? rating;
  int? pokemonId;

  SendRatingEntity({this.id, this.rating, this.pokemonId});
}
