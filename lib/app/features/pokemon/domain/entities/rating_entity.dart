class RatingEntity {
  int? id;
  int? rating;
  int? pokemonId;

  RatingEntity({this.id, this.rating, this.pokemonId});
}
