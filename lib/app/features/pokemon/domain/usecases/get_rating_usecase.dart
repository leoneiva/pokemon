import 'package:dartz/dartz.dart';

import '../../../../core/domain/errors/errors.dart';
import '../entities/rating_entity.dart';

abstract class GetRatingUsecase {
  Future<Either<Failure, RatingEntity>> call({
    required int id,
  });
}
