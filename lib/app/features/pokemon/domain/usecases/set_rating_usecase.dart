import 'package:dartz/dartz.dart';

import '../../../../core/domain/errors/errors.dart';
import '../entities/send_rating_entity.dart';

abstract class SetRatingUsecase {
  Future<Either<Failure, void>> call({
    required SendRatingEntity rating,
  });
}
