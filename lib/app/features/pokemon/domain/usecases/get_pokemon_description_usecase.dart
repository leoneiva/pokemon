import 'package:dartz/dartz.dart';

import '../../../../core/domain/errors/errors.dart';

abstract class GetPokemonDescriptionUsecase {
  Future<Either<Failure, String>> call({
    required int id,
  });
}
