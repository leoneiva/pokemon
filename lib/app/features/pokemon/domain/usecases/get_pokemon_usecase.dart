import 'package:dartz/dartz.dart';

import '../../../../core/domain/errors/errors.dart';
import '../entities/pokemon_entity.dart';

abstract class GetPokemonUsecase {
  Future<Either<Failure, PokemonEntity>> call({
    required int id,
  });
}
