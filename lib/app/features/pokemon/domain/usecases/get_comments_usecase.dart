import 'package:dartz/dartz.dart';

import '../../../../core/domain/errors/errors.dart';
import '../entities/comment_entity.dart';

abstract class GetCommentsUsecase {
  Future<Either<Failure, List<CommentEntity>>> call({
    required int id,
  });
}
