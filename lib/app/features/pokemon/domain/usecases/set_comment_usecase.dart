import 'package:dartz/dartz.dart';

import '../../../../core/domain/errors/errors.dart';
import '../entities/send_comment_entity.dart';

abstract class SetCommentUsecase {
  Future<Either<Failure, void>> call({
    required SendCommentEntity comment,
  });
}
