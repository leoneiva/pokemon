import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../entities/rating_entity.dart';

abstract class GetRatingRepository {
  Future<Either<Failure, RatingEntity>> getRating({
    required int id,
  });
}
