import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../entities/send_rating_entity.dart';

abstract class PutRatingRepository {
  Future<Either<Failure, void>> putRating({
    required SendRatingEntity rating,
  });
}
