import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';

abstract class GetPokemonDescriptionRepository {
  Future<Either<Failure, String>> getPokemonDescription({
    required int id,
  });
}
