import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../entities/pokemon_entity.dart';

abstract class GetPokemonRepository {
  Future<Either<Failure, PokemonEntity>> getPokemon({
    required int id,
  });
}
