import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../entities/send_comment_entity.dart';

abstract class SetCommentRepository {
  Future<Either<Failure, void>> setComment({
    required SendCommentEntity comment,
  });
}
