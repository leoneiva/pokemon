import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../entities/send_rating_entity.dart';

abstract class SetRatingRepository {
  Future<Either<Failure, void>> setRating({
    required SendRatingEntity rating,
  });
}
