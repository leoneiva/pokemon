import 'package:dartz/dartz.dart';
import '../../../../core/domain/errors/errors.dart';
import '../entities/comment_entity.dart';

abstract class GetCommentsRepository {
  Future<Either<Failure, List<CommentEntity>>> getComments({
    required int id,
  });
}
