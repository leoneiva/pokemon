import 'package:pokemon/app/core/services/database/db_service.dart';
import '../../infra/datasources/put_rating_datasource.dart';
import '../../infra/models/send_rating_model.dart';

class PutRatingDataSourceImpl implements PutRatingDataSource {
  DBService db;
  PutRatingDataSourceImpl(this.db);
  @override
  Future<void> putRating({
    required SendRatingModel rating,
  }) async {
    await db.putRating(rating: rating);
  }
}
