import 'package:http/http.dart';
import '../../../../core/constants/api.dart';
import '../../infra/datasources/get_pokemon_datasource.dart';
import '../../infra/models/pokemon_model.dart';

class GetPokemonDataSourceImpl implements GetPokemonDataSource {
  final Client _http;
  GetPokemonDataSourceImpl(this._http);
  @override
  Future<PokemonModel> getPokemon({
    required int id,
  }) async {
    Response response = await _http.get(
      Uri.https(
        Api.base.path,
        Api.pokemons.path + id.toString(),
      ),
    );
    return PokemonModel.fromJson(response.body);
  }
}
