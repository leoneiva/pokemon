import 'dart:convert';

import 'package:http/http.dart';
import '../../../../core/constants/api.dart';
import '../../infra/datasources/get_pokemon_description_datasource.dart';

class GetPokemonDescriptionDataSourceImpl
    implements GetPokemonDescriptionDataSource {
  final Client _http;
  GetPokemonDescriptionDataSourceImpl(this._http);
  @override
  Future<String> getPokemonDescription({
    required int id,
  }) async {
    Response response = await _http.get(
      Uri.https(
        Api.base.path,
        Api.pokemonDescription.path + id.toString(),
      ),
    );
    var data = jsonDecode(response.body);
    return data['effect_entries'][0]['effect'].toString();
  }
}
