import 'package:pokemon/app/core/services/database/db_service.dart';
import '../../infra/datasources/set_rating_datasource.dart';
import '../../infra/models/send_rating_model.dart';

class SetRatingDataSourceImpl implements SetRatingDataSource {
  DBService db;
  SetRatingDataSourceImpl(this.db);
  @override
  Future<void> setRating({
    required SendRatingModel rating,
  }) async {
    await db.setRating(rating: rating);
  }
}
