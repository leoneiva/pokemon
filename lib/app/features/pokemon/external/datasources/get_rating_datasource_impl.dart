import 'package:pokemon/app/core/services/database/db_service.dart';
import '../../infra/datasources/get_rating_datasource.dart';
import '../../infra/models/rating_model.dart';

class GetRatingDataSourceImpl implements GetRatingDataSource {
  DBService db;
  GetRatingDataSourceImpl(this.db);
  @override
  Future<RatingModel> getRating({
    required int id,
  }) async {
    var consult = await db.getRating(idPokemon: id);
    return RatingModel.fromMap(consult!.first);
  }
}
