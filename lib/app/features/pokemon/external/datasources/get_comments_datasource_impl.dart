import 'package:pokemon/app/core/services/database/db_service.dart';
import '../../infra/datasources/get_comments_datasource.dart';
import '../../infra/models/comment_model.dart';

class GetCommentsDataSourceImpl implements GetCommentsDataSource {
  DBService db;
  GetCommentsDataSourceImpl(this.db);
  @override
  Future<List<CommentModel>> getComments({
    required int id,
  }) async {
    return await db.getComments(idPokemon: id);
  }
}
