import 'package:pokemon/app/core/services/database/db_service.dart';
import '../../infra/datasources/set_comment_datasource.dart';
import '../../infra/models/send_comment_model.dart';

class SetCommentDataSourceImpl implements SetCommentDataSource {
  DBService db;
  SetCommentDataSourceImpl(this.db);
  @override
  Future<void> setComment({
    required SendCommentModel comment,
  }) async {
    await db.setComment(comment: comment);
  }
}
