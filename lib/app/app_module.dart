import 'package:flutter_modular/flutter_modular.dart';

import 'features/dashboard/dashboard_module.dart';
import 'features/initialization/initialization_module.dart';
import 'features/pokemon/pokemon_module.dart';

class AppModule extends Module {
  @override
  List<Bind<Object>> get binds => [];

  @override
  List<ModularRoute> get routes => [
        ModuleRoute(
          Modular.initialRoute,
          module: InitializationModule(),
        ),
        ModuleRoute(
          Modular.initialRoute,
          module: DashboardModule(),
        ),
        ModuleRoute(
          Modular.initialRoute,
          module: PokemonModule(),
        ),
      ];
}
