import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:pokemon/app/core/constants/keys/initialization_keys_test.dart';
import 'package:pokemon/app/core/utils/custom_navigator.dart';
import 'package:pokemon/app/features/initialization/presenter/pages/splash_page.dart';

class CustomNavigatorMock extends Mock implements CustomNavigator {}

void main() {
  testWidgets('Splash Page Test', (WidgetTester tester) async {
    final customNavigator = CustomNavigatorMock();

    await tester.runAsync(() async {
      await tester.pumpWidget(
        MaterialApp(
          home: SplashPage(navigator: customNavigator),
        ),
      );

      await tester.pumpAndSettle();
      // Widgets
      expect(find.byType(SafeArea), findsOneWidget);
      expect(find.byType(Scaffold), findsOneWidget);
      expect(find.byType(Column), findsOneWidget);
      expect(find.byType(Image), findsNWidgets(2));
      // keys
      expect(find.byKey(Key(AppKeys.background.path)), findsOneWidget);
      expect(find.byKey(Key(AppKeys.logo.path)), findsOneWidget);
    });
  });
}
