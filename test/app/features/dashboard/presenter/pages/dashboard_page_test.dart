import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:pokemon/app/core/utils/custom_navigator.dart';
import 'package:pokemon/app/features/dashboard/domain/entities/get_pokemons_entity.dart';
import 'package:pokemon/app/features/dashboard/domain/entities/pokemons_entity.dart';
import 'package:pokemon/app/features/dashboard/domain/usecases/get_pokemons_usecase.dart';
import 'package:pokemon/app/features/dashboard/presenter/controllers/dashboard_controller.dart';
import 'package:pokemon/app/features/dashboard/presenter/pages/dashboard_page.dart';
import 'package:pokemon/app/features/dashboard/presenter/widgets/poke_app_bar_widget.dart';
import 'package:scroll_edge_listener/scroll_edge_listener.dart';

class GetPokemonsUsecaseMock extends Mock implements GetPokemonsUsecase {}

class CustomNavigatorMock extends Mock implements CustomNavigator {}

void main() {
  testWidgets('Dashboard Page Test', (WidgetTester tester) async {
    final getPokemonsUsecase = GetPokemonsUsecaseMock();
    final customNavigator = CustomNavigatorMock();
    final controller = DashboardController(
      getPokemonsUsecase,
    );

    when(
      () => getPokemonsUsecase.call(
          params: GetPokemonsEntity(
        limit: 10,
        offset: 10,
      )),
    ).thenAnswer(
      (_) async => Right(PokemonsEntity(
        count: 1,
        next: 'next_test',
        previous: 'previous_test',
        results: [PokemonEntity(name: 'name_test', url: 'url_test')],
      )),
    );

    await tester.runAsync(() async {
      await tester.pumpWidget(
        MaterialApp(
          home: ScreenUtilInit(
            designSize: const Size(393, 852),
            builder: (BuildContext context, _) => DashboardPage(
              controller: controller,
              navigator: customNavigator,
            ),
          ),
        ),
      );

      await tester.pumpAndSettle();

      // Widgets
      expect(find.byType(SafeArea), findsNWidgets(2));
      expect(find.byType(Scaffold), findsOneWidget);
      expect(find.byType(Observer), findsOneWidget);
      expect(find.byType(Visibility), findsNWidgets(3));
      expect(find.byType(PokeAppBarWidget), findsOneWidget);
      expect(find.byType(ScrollEdgeListener), findsOneWidget);
      expect(find.byType(GridView), findsOneWidget);
      expect(find.byType(Visibility), findsNWidgets(3));
      expect(find.byType(Expanded), findsOneWidget);
      expect(find.byType(Column), findsOneWidget);
    });
  });
}
