import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:pokemon/app/features/dashboard/domain/entities/get_pokemons_entity.dart';
import 'package:pokemon/app/features/dashboard/domain/entities/pokemons_entity.dart';
import 'package:pokemon/app/features/dashboard/domain/usecases/get_pokemons_usecase.dart';
import 'package:pokemon/app/features/dashboard/presenter/controllers/dashboard_controller.dart';

class GetPokemonsUsecaseMock extends Mock implements GetPokemonsUsecase {}

void main() {
  group('Dashboard controller test', () {
    final PokemonsEntity pokemons = PokemonsEntity(
      count: 1,
      next: 'next_test',
      previous: 'previous_test',
      results: [PokemonEntity(name: 'name_test', url: 'url_test')],
    );

    final GetPokemonsEntity params = GetPokemonsEntity(
      limit: 18,
      offset: 0,
    );

    test('Test get pokemons', () async {
      final getPokemonsUsecase = GetPokemonsUsecaseMock();
      final controller = DashboardController(
        getPokemonsUsecase,
      );

      when(
        () => getPokemonsUsecase.call(params: params),
      ).thenAnswer(
        (_) async => Right(pokemons),
      );

      await controller.getPokemons();

      verify(() => getPokemonsUsecase.call(params: params)).called(1);
      expect(controller.pokemons, pokemons);
    });

    test('Test load more pokemons', () async {
      final getPokemonsUsecase = GetPokemonsUsecaseMock();
      final controller = DashboardController(
        getPokemonsUsecase,
      );

      when(
        () => getPokemonsUsecase.call(params: params),
      ).thenAnswer(
        (_) async => Right(pokemons),
      );

      await controller.getPokemons();

      verify(() => getPokemonsUsecase.call(params: params)).called(1);
      expect(controller.pokemons, pokemons);
    });
  });
}
