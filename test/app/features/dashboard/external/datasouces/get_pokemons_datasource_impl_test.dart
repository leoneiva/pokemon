import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:http/http.dart';
import 'package:pokemon/app/core/constants/api.dart';
import 'package:pokemon/app/features/dashboard/domain/entities/get_pokemons_entity.dart';
import 'package:pokemon/app/features/dashboard/external/datasources/get_pokemons_datasource_impl.dart';
import 'package:pokemon/app/features/dashboard/infra/models/pokemons_model.dart';

class ClientMock extends Mock implements Client {}

void main() {
  group('Get pokemon dataSource impl ...', () {
    test(
      "Should return a PokemonModel",
      (() async {
        final pokemonEntity = GetPokemonsEntity(limit: 10, offset: 10);
        final response = Response(body, 200);
        final params = Uri.https(
          Api.base.path,
          Api.pokemons.path,
          pokemonEntity.toJson(),
        );
        final clientMock = ClientMock();
        final dataSource = GetPokemonsDataSourceImpl(clientMock);

        when(() => clientMock.get(params)).thenAnswer(
          (_) async => response,
        );
        final responsePokemons = await dataSource.getPokemons(
          params: pokemonEntity,
        );
        expect(responsePokemons, isA<PokemonsModel>());
      }),
    );
  });
}

var body =
    '{"count":1279,"next":"https://pokeapi.co/api/v2/pokemon/?offset=20&limit=20","previous":null,"results":[{"name":"bulbasaur","url":"https://pokeapi.co/api/v2/pokemon/1/"},{"name":"ivysaur","url":"https://pokeapi.co/api/v2/pokemon/2/"},{"name":"venusaur","url":"https://pokeapi.co/api/v2/pokemon/3/"},{"name":"charmander","url":"https://pokeapi.co/api/v2/pokemon/4/"},{"name":"charmeleon","url":"https://pokeapi.co/api/v2/pokemon/5/"},{"name":"charizard","url":"https://pokeapi.co/api/v2/pokemon/6/"},{"name":"squirtle","url":"https://pokeapi.co/api/v2/pokemon/7/"},{"name":"wartortle","url":"https://pokeapi.co/api/v2/pokemon/8/"},{"name":"blastoise","url":"https://pokeapi.co/api/v2/pokemon/9/"},{"name":"caterpie","url":"https://pokeapi.co/api/v2/pokemon/10/"},{"name":"metapod","url":"https://pokeapi.co/api/v2/pokemon/11/"},{"name":"butterfree","url":"https://pokeapi.co/api/v2/pokemon/12/"},{"name":"weedle","url":"https://pokeapi.co/api/v2/pokemon/13/"},{"name":"kakuna","url":"https://pokeapi.co/api/v2/pokemon/14/"},{"name":"beedrill","url":"https://pokeapi.co/api/v2/pokemon/15/"},{"name":"pidgey","url":"https://pokeapi.co/api/v2/pokemon/16/"},{"name":"pidgeotto","url":"https://pokeapi.co/api/v2/pokemon/17/"},{"name":"pidgeot","url":"https://pokeapi.co/api/v2/pokemon/18/"},{"name":"rattata","url":"https://pokeapi.co/api/v2/pokemon/19/"},{"name":"raticate","url":"https://pokeapi.co/api/v2/pokemon/20/"}]}';
