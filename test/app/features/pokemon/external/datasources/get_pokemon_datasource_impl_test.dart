import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:http/http.dart';
import 'package:pokemon/app/core/constants/api.dart';
import 'package:pokemon/app/features/pokemon/external/datasources/get_pokemon_datasource_impl.dart';
import 'package:pokemon/app/features/pokemon/infra/models/pokemon_model.dart';

class ClientMock extends Mock implements Client {}

void main() {
  group('Get pokemon dataSource impl ...', () {
    test(
      "Should return a PokemonModel",
      (() async {
        final response = Response(body, 200);
        final params = Uri.https(
          Api.base.path,
          '${Api.pokemons.path}1',
        );
        final clientMock = ClientMock();
        final dataSource = GetPokemonDataSourceImpl(clientMock);

        when(() => clientMock.get(params)).thenAnswer(
          (_) async => response,
        );
        final responsePokemon = await dataSource.getPokemon(id: 1);
        expect(responsePokemon, isA<PokemonModel>());
      }),
    );
  });
}

var body =
    '{"moves": [{"move": {"name": "razor-wind","url": "https://pokeapi.co/api/v2/move/13/"},"version_group_details": [{"level_learned_at": 0,"move_learn_method": {"name": "egg","url": "https://pokeapi.co/api/v2/move-learn-method/2/"},"version_group": {"name": "gold-silver","url": "https://pokeapi.co/api/v2/version-group/3/"}},{"level_learned_at": 0,"move_learn_method": {"name": "egg","url": "https://pokeapi.co/api/v2/move-learn-method/2/"},"version_group": {"name": "crystal","url": "https://pokeapi.co/api/v2/version-group/4/"}}]}],"stats": [{"base_stat": 45,"effort": 0,"stat": {"name": "hp","url": "https://pokeapi.co/api/v2/stat/1/"}}],"types": [{"slot": 1,"type": {"name": "grass","url": "https://pokeapi.co/api/v2/type/12/"}},{"slot": 2,"type": {"name": "poison","url": "https://pokeapi.co/api/v2/type/4/"}}],"weight": 69,"height": 7}';
