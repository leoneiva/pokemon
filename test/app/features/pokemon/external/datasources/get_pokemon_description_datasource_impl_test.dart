import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:http/http.dart';
import 'package:pokemon/app/core/constants/api.dart';
import 'package:pokemon/app/features/pokemon/external/datasources/get_pokemon_description_datasource_impl.dart';

class ClientMock extends Mock implements Client {}

void main() {
  group('Get Description dataSource impl ...', () {
    test(
      "Should return a String",
      (() async {
        final response = Response(body, 200);
        final params = Uri.https(
          Api.base.path,
          '${Api.pokemonDescription.path}1',
        );
        final clientMock = ClientMock();
        final dataSource = GetPokemonDescriptionDataSourceImpl(clientMock);

        when(() => clientMock.get(params)).thenAnswer(
          (_) async => response,
        );
        final responseDescription =
            await dataSource.getPokemonDescription(id: 1);
        expect(responseDescription, isA<String>());
      }),
    );
  });
}

var body = '{"effect_entries":[{"effect":"Hat im Kampf keinen Effekt."}]}';
