// ignore_for_file: void_checks

import 'dart:ffi';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:pokemon/app/core/domain/errors/errors.dart';
import 'package:pokemon/app/core/utils/custom_navigator.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/comment_entity.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/pokemon_entity.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/rating_entity.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/send_comment_entity.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/send_rating_entity.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/get_comments_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/get_pokemon_description_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/get_pokemon_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/get_rating_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/put_rating_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/set_comment_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/set_rating_usecase.dart';
import 'package:pokemon/app/features/pokemon/presenter/controllers/pokemon_controller.dart';

class GetPokemonUsecaseMock extends Mock implements GetPokemonUsecase {}

class GetRatingUsecaseMock extends Mock implements GetRatingUsecase {}

class SetRatingUsecaseMock extends Mock implements SetRatingUsecase {}

class PutRatingUsecaseMock extends Mock implements PutRatingUsecase {}

class SetCommentUsecaseMock extends Mock implements SetCommentUsecase {}

class GetCommentsUsecaseMock extends Mock implements GetCommentsUsecase {}

class CustomNavigatorMock extends Mock implements CustomNavigator {}

class GetPokemonDescriptionUsecaseMock extends Mock
    implements GetPokemonDescriptionUsecase {}

final PokemonEntity pokemon = PokemonEntity(
  description: 'teste',
  height: 100,
  moves: [
    MovesEntity(
      move: MoveEntity(
        name: 'move_test',
        url: 'url_test',
      ),
    ),
    MovesEntity(
      move: MoveEntity(
        name: 'move_test',
        url: 'url_test',
      ),
    ),
  ],
  stats: [
    StatsEntity(
      baseStat: 1,
      effort: 1,
      stat: MoveEntity(
        name: 'move_test',
        url: 'url_test',
      ),
    ),
    StatsEntity(
      baseStat: 1,
      effort: 1,
      stat: MoveEntity(
        name: 'move_test',
        url: 'url_test',
      ),
    ),
    StatsEntity(
      baseStat: 1,
      effort: 1,
      stat: MoveEntity(
        name: 'move_test',
        url: 'url_test',
      ),
    ),
    StatsEntity(
      baseStat: 1,
      effort: 1,
      stat: MoveEntity(
        name: 'move_test',
        url: 'url_test',
      ),
    ),
    StatsEntity(
      baseStat: 1,
      effort: 1,
      stat: MoveEntity(
        name: 'move_test',
        url: 'url_test',
      ),
    ),
    StatsEntity(
      baseStat: 1,
      effort: 1,
      stat: MoveEntity(
        name: 'move_test',
        url: 'url_test',
      ),
    )
  ],
  types: [
    TypesEntity(
      slot: 1,
      type: MoveEntity(
        name: 'move_test',
        url: 'url_test',
      ),
    ),
    TypesEntity(
      slot: 1,
      type: MoveEntity(
        name: 'move_test',
        url: 'url_test',
      ),
    ),
  ],
  weight: 100,
);

const String description = 'test_description';
const String errorMessage = 'test_error_message';

void main() {
  group('Pokemon controller test', () {
    final getPokemonUsecase = GetPokemonUsecaseMock();
    final getPokemonDescriptionUsecase = GetPokemonDescriptionUsecaseMock();
    final getRatingUsecase = GetRatingUsecaseMock();
    final setRatingUsecase = SetRatingUsecaseMock();
    final putRatingUsecase = PutRatingUsecaseMock();
    final setCommentUsecase = SetCommentUsecaseMock();
    final getCommentsUsecase = GetCommentsUsecaseMock();
    final customNavigator = CustomNavigatorMock();

    // Test Get Pokemon
    test('Test get pokemon', () async {
      when(
        () => getPokemonUsecase.call(id: 1),
      ).thenAnswer(
        (_) async => Right(pokemon),
      );

      final controller = PokemonController(
        getPokemonUsecase,
        getPokemonDescriptionUsecase,
        getRatingUsecase,
        setRatingUsecase,
        putRatingUsecase,
        setCommentUsecase,
        getCommentsUsecase,
        customNavigator,
      );

      await controller.getPokemon(idPokemon: 1);

      verify(() => getPokemonUsecase(id: 1)).called(1);
      expect(controller.pokemon, pokemon);
    });

    test('Test error Not found 404 when get pokemon', () async {
      when(
        () => getPokemonUsecase.call(id: 1),
      ).thenAnswer(
        (_) async => Left(NotFoundError(message: errorMessage)),
      );

      final controller = PokemonController(
          getPokemonUsecase,
          getPokemonDescriptionUsecase,
          getRatingUsecase,
          setRatingUsecase,
          putRatingUsecase,
          setCommentUsecase,
          getCommentsUsecase,
          customNavigator);

      await controller.getPokemon(idPokemon: 1);

      verify(() => getPokemonUsecase(id: 1)).called(1);
      expect(NotFoundError, NotFoundError);
    });

    test('Test error Timeout 408 when get pokemon', () async {
      when(
        () => getPokemonUsecase.call(id: 1),
      ).thenAnswer(
        (_) async => Left(TimeOutError(message: errorMessage)),
      );

      final controller = PokemonController(
          getPokemonUsecase,
          getPokemonDescriptionUsecase,
          getRatingUsecase,
          setRatingUsecase,
          putRatingUsecase,
          setCommentUsecase,
          getCommentsUsecase,
          customNavigator);

      await controller.getPokemon(idPokemon: 1);

      verify(() => getPokemonUsecase(id: 1)).called(1);
      expect(TimeOutError, TimeOutError);
    });

    test('Test error Internal Error 500 when get pokemon', () async {
      when(
        () => getPokemonUsecase.call(id: 1),
      ).thenAnswer(
        (_) async => Left(InternalError(message: errorMessage)),
      );

      final controller = PokemonController(
          getPokemonUsecase,
          getPokemonDescriptionUsecase,
          getRatingUsecase,
          setRatingUsecase,
          putRatingUsecase,
          setCommentUsecase,
          getCommentsUsecase,
          customNavigator);

      await controller.getPokemon(idPokemon: 1);

      verify(() => getPokemonUsecase(id: 1)).called(1);
      expect(InternalError, InternalError);
    });

    // Test Description

    test('Test get pokemon description', () async {
      when(
        () => getPokemonDescriptionUsecase.call(id: 1),
      ).thenAnswer(
        (_) async => const Right(description),
      );

      final controller = PokemonController(
          getPokemonUsecase,
          getPokemonDescriptionUsecase,
          getRatingUsecase,
          setRatingUsecase,
          putRatingUsecase,
          setCommentUsecase,
          getCommentsUsecase,
          customNavigator);

      await controller.getPokemonDescription(idPokemon: 1);

      verify(() => getPokemonDescriptionUsecase(id: 1)).called(1);
      expect(controller.pokemon.description, description);
    });

    test('Test error Internal Error 500 when get pokemon description',
        () async {
      when(
        () => getPokemonDescriptionUsecase.call(id: 1),
      ).thenAnswer(
        (_) async => Left(InternalError(message: errorMessage)),
      );

      final controller = PokemonController(
          getPokemonUsecase,
          getPokemonDescriptionUsecase,
          getRatingUsecase,
          setRatingUsecase,
          putRatingUsecase,
          setCommentUsecase,
          getCommentsUsecase,
          customNavigator);

      await controller.getPokemonDescription(idPokemon: 1);

      verify(() => getPokemonDescriptionUsecase(id: 1)).called(1);
      expect(InternalError, InternalError);
    });

    test('Test error Timeout 408 when get pokemon description', () async {
      when(
        () => getPokemonDescriptionUsecase.call(id: 1),
      ).thenAnswer(
        (_) async => Left(TimeOutError(message: errorMessage)),
      );

      final controller = PokemonController(
          getPokemonUsecase,
          getPokemonDescriptionUsecase,
          getRatingUsecase,
          setRatingUsecase,
          putRatingUsecase,
          setCommentUsecase,
          getCommentsUsecase,
          customNavigator);

      await controller.getPokemonDescription(idPokemon: 1);

      verify(() => getPokemonDescriptionUsecase(id: 1)).called(1);
      expect(TimeOutError, TimeOutError);
    });

    test('Test error Not Found 404 when get pokemon description', () async {
      when(
        () => getPokemonDescriptionUsecase.call(id: 1),
      ).thenAnswer(
        (_) async => Left(NotFoundError(message: errorMessage)),
      );

      final controller = PokemonController(
          getPokemonUsecase,
          getPokemonDescriptionUsecase,
          getRatingUsecase,
          setRatingUsecase,
          putRatingUsecase,
          setCommentUsecase,
          getCommentsUsecase,
          customNavigator);

      await controller.getPokemonDescription(idPokemon: 1);

      verify(() => getPokemonDescriptionUsecase(id: 1)).called(1);
      expect(NotFoundError, NotFoundError);
    });

    // Test Comments
    test('Test get comments', () async {
      List<CommentEntity> list = [
        CommentEntity(comment: 'Test', id: 1, pokemonId: 1)
      ];

      when(
        () => getCommentsUsecase.call(id: 1),
      ).thenAnswer(
        (_) async => Right(list),
      );

      final controller = PokemonController(
          getPokemonUsecase,
          getPokemonDescriptionUsecase,
          getRatingUsecase,
          setRatingUsecase,
          putRatingUsecase,
          setCommentUsecase,
          getCommentsUsecase,
          customNavigator);

      await controller.getComments(idPokemon: 1);

      verify(() => getCommentsUsecase(id: 1)).called(1);
      expect(controller.comments, list);
    });

    // Test Rating
    test('Test get rating', () async {
      RatingEntity rating = RatingEntity(id: 1, pokemonId: 1, rating: 5);

      when(
        () => getRatingUsecase.call(id: 1),
      ).thenAnswer(
        (_) async => Right(rating),
      );

      final controller = PokemonController(
          getPokemonUsecase,
          getPokemonDescriptionUsecase,
          getRatingUsecase,
          setRatingUsecase,
          putRatingUsecase,
          setCommentUsecase,
          getCommentsUsecase,
          customNavigator);

      await controller.getRating(idPokemon: 1);

      verify(() => getRatingUsecase(id: 1)).called(1);
      expect(controller.ratingPokemon, rating);
    });
  });
}
