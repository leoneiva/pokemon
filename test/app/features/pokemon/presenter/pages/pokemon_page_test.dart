// ignore_for_file: void_checks

import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:network_image_mock/network_image_mock.dart';
import 'package:pokemon/app/core/utils/custom_navigator.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/comment_entity.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/pokemon_entity.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/pokemon_page_entity.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/rating_entity.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/send_comment_entity.dart';
import 'package:pokemon/app/features/pokemon/domain/entities/send_rating_entity.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/get_comments_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/get_pokemon_description_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/get_pokemon_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/get_rating_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/put_rating_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/set_comment_usecase.dart';
import 'package:pokemon/app/features/pokemon/domain/usecases/set_rating_usecase.dart';
import 'package:pokemon/app/features/pokemon/presenter/controllers/pokemon_controller.dart';
import 'package:pokemon/app/features/pokemon/presenter/pages/pokemon_page.dart';
import 'package:pokemon/app/features/pokemon/presenter/widgets/pokemon_about_widget.dart';
import 'package:pokemon/app/features/pokemon/presenter/widgets/pokemon_comments_widget.dart';
import 'package:pokemon/app/features/pokemon/presenter/widgets/pokemon_description_widget.dart';

class GetPokemonUsecaseMock extends Mock implements GetPokemonUsecase {}

class GetRatingUsecaseMock extends Mock implements GetRatingUsecase {}

class SetRatingUsecaseMock extends Mock implements SetRatingUsecase {}

class PutRatingUsecaseMock extends Mock implements PutRatingUsecase {}

class SetCommentUsecaseMock extends Mock implements SetCommentUsecase {}

class GetCommentsUsecaseMock extends Mock implements GetCommentsUsecase {}

class CustomNavigatorMock extends Mock implements CustomNavigator {}

class GetPokemonDescriptionUsecaseMock extends Mock
    implements GetPokemonDescriptionUsecase {}

void main() {
  final PokemonEntity pokemonEntity = PokemonEntity(
    description: 'teste',
    height: 100,
    moves: [
      MovesEntity(
        move: MoveEntity(
          name: 'move_test',
          url: 'url_test',
        ),
      ),
      MovesEntity(
        move: MoveEntity(
          name: 'move_test',
          url: 'url_test',
        ),
      ),
    ],
    stats: [
      StatsEntity(
        baseStat: 1,
        effort: 1,
        stat: MoveEntity(
          name: 'move_test',
          url: 'url_test',
        ),
      ),
      StatsEntity(
        baseStat: 1,
        effort: 1,
        stat: MoveEntity(
          name: 'move_test',
          url: 'url_test',
        ),
      ),
      StatsEntity(
        baseStat: 1,
        effort: 1,
        stat: MoveEntity(
          name: 'move_test',
          url: 'url_test',
        ),
      ),
      StatsEntity(
        baseStat: 1,
        effort: 1,
        stat: MoveEntity(
          name: 'move_test',
          url: 'url_test',
        ),
      ),
      StatsEntity(
        baseStat: 1,
        effort: 1,
        stat: MoveEntity(
          name: 'move_test',
          url: 'url_test',
        ),
      ),
      StatsEntity(
        baseStat: 1,
        effort: 1,
        stat: MoveEntity(
          name: 'move_test',
          url: 'url_test',
        ),
      )
    ],
    types: [
      TypesEntity(
        slot: 1,
        type: MoveEntity(
          name: 'move_test',
          url: 'url_test',
        ),
      ),
      TypesEntity(
        slot: 1,
        type: MoveEntity(
          name: 'move_test',
          url: 'url_test',
        ),
      ),
    ],
    weight: 100,
  );
  testWidgets('Pokemon Page Test', (WidgetTester tester) async {
    final getPokemonUsecase = GetPokemonUsecaseMock();
    final getRatingUsecase = GetRatingUsecaseMock();
    final getPokemonDescriptionUsecase = GetPokemonDescriptionUsecaseMock();
    final setRatingUsecase = SetRatingUsecaseMock();
    final putRatingUsecase = PutRatingUsecaseMock();
    final setCommentUsecase = SetCommentUsecaseMock();
    final getCommentsUsecase = GetCommentsUsecaseMock();
    final customNavigator = CustomNavigatorMock();

    final controller = PokemonController(
      getPokemonUsecase,
      getPokemonDescriptionUsecase,
      getRatingUsecase,
      setRatingUsecase,
      putRatingUsecase,
      setCommentUsecase,
      getCommentsUsecase,
      customNavigator,
    );

    when(
      () => getCommentsUsecase(id: 1),
    ).thenAnswer(
      (_) async => Right([CommentEntity(comment: 'Test', id: 1, pokemonId: 1)]),
    );

    when(
      () => setCommentUsecase(
        comment: SendCommentEntity(pokemonId: 1, comment: 'comment'),
      ),
    ).thenAnswer(
      (_) async => const Right(true),
    );

    when(
      () => getRatingUsecase(id: 1),
    ).thenAnswer(
      (_) async => Right(RatingEntity(id: 1, pokemonId: 1, rating: 5)),
    );

    when(
      () => setRatingUsecase(
        rating: SendRatingEntity(
          id: 1,
          rating: 3,
          pokemonId: 1,
        ),
      ),
    ).thenAnswer(
      (_) async => const Right(true),
    );

    when(
      () => putRatingUsecase(
        rating: SendRatingEntity(
          id: 1,
          rating: 3,
          pokemonId: 1,
        ),
      ),
    ).thenAnswer(
      (_) async => const Right(true),
    );

    when(
      () => getPokemonUsecase.call(id: 1),
    ).thenAnswer(
      (_) async => Right(
        pokemonEntity,
      ),
    );

    when(
      () => getPokemonDescriptionUsecase.call(id: 1),
    ).thenAnswer(
      (_) async => const Right('description_mock'),
    );

    controller.pokemon = pokemonEntity;

    await mockNetworkImagesFor(() => tester.runAsync(() async {
          await tester.pumpWidget(
            MaterialApp(
              home: ScreenUtilInit(
                designSize: const Size(393, 852),
                builder: (BuildContext context, _) => PokemonPage(
                  pokemonPageInfo: PokemonPageEntity(
                    id: 1,
                    image:
                        'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/1.png',
                    name: 'bulbasaur',
                  ),
                  controller: controller,
                ),
              ),
            ),
          );

          await tester.pumpAndSettle();
          expect(find.byType(SafeArea), findsNWidgets(2));
          expect(find.byType(Scaffold), findsOneWidget);
          expect(find.byType(Positioned), findsOneWidget);
          expect(find.byType(Observer), findsOneWidget);
          expect(find.byType(AppBar), findsOneWidget);
          expect(find.byType(ListView), findsNWidgets(2));
          expect(find.byType(Image), findsNWidgets(2));
          expect(find.byType(PokemonAboutWidget), findsOneWidget);
          expect(find.byType(PokemonStatusWidget), findsOneWidget);
          expect(find.byType(PokemonCommentsWidget), findsOneWidget);
        }));
  });
}
